#! /usr/bin/env python3

import os
import re
import time
import json

import numpy as np

class ValidateJson:
    def __init__(self):
        self.input_files_path = "results/" + time.strftime("%Y%m%d_%H", time.localtime()) + "/trace_data/"
        self.data = np.zeros(1000000, dtype=str)
        self.json_data = np.zeros(1000000, dtype=str)



    def read_files(self):
        print("Reading logs...")
        # get list of all files in trace_data folder
        files = [self.input_files_path + file_name for file_name in os.listdir(self.input_files_path)]
        # read log files
        for f in files:
            with open(f, 'r') as in_file:
                self.data.append(in_file.readlines())



    # loop through to make them valid
    def validate_json(self):
        print("Making logs valid...")
        # make it a valid json log file
        for f in self.data:
            file_string = "{"
            for i, line in enumerate(f):
                matches = re.search(r'(\"\d+_\d+(\.\d+)?\")(.*)', line)
                file_string = file_string + matches.group(1) + ":" + matches.group(3)

                if i != len(f) - 1:
                    file_string = file_string + ","

            file_string = file_string + "}"
            self.json_data.append(file_string)
        return self.json_data



if __name__ == '__main__':
    vj = ValidateJson()
    vj.read_files()
    vj.validate_json()
