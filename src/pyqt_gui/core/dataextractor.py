import glob

import pandas as pd

import core.utils as cu
import core.entity as ce

class DataExtractor:
    def __init__(self):
        self.entity = ce.Entity()

    def extract_trace_data(self, data_path):
        # get list of all files in df folder
        # read log files
        df = pd.concat(
                [
                    pd.read_csv(
                        f, names = [
                            'rvId', 'hvId', 'simTime',
                            'kalman.x', 'kalman.y',
                            'pos.x', 'pos.y',
                            'gps.x', 'gps.y',
                            'posSD.x', 'posSD.y',
                            'vel.x', 'vel.y',
                            'distance', 'distanceSD',
                            'ratings.ewma.ART', 'ratings.ewma.MBF',
                            'ratings.ewma.SAW', 'ratings.ewma.MVP',
                            'ratings.ewma.MVN', 'ratings.ewma.MAP',
                            'ratings.ewma.MAN', 'ratings.ewma.MDM',
                            'ratings.ewma.KALMAN', 'ratings.ewma.PSA',
                            'ratings.ewma.SA', 'ratings.ewma.PO',
                            'ratings.ewma.MDT', 'ratings.ewma.BA',
                            'ratings.ewma.LP', 'ratings.ewma.MP1',
                            'ratings.ewma.MP2', 'ratings.ewma.FBR',
                            'ratings.ewma.suspicion',
                            'ratings.instant.ART', 'ratings.instant.MBF',
                            'ratings.instant.SAW', 'ratings.instant.MVP',
                            'ratings.instant.MVN', 'ratings.instant.MAP',
                            'ratings.instant.MAN', 'ratings.instant.MDM',
                            'ratings.instant.KALMAN', 'ratings.instant.PSA',
                            'ratings.instant.SA', 'ratings.instant.PO',
                            'ratings.instant.MDT', 'ratings.instant.BA',
                            'ratings.instant.LP', 'ratings.instant.MP1',
                            'ratings.instant.MP2', 'ratings.instant.FBR',
                            'ratings.instant.suspicion',
                            'suspectedAttack',
                            'eebl.mpsOff', 'eebl.mpsOn',
                            'rssi',
                            'ewma.mlPrediction', 'instant.mlPrediction',
                            'wsmData',
                            'latency.simTime', 'latency.realTime', 'latency.bsm',
                            'latency.distance',
                            'execTime.ART', 'execTime.SAW', 'execTime.MBF',
                            'execTime.MVP', 'execTime.MVN', 'execTime.MAP',
                            'execTime.MAN', 'execTime.FBR', 'execTIme.PSA',
                            'execTime.SA', 'execTime.PO', 'execTime.MDT',
                            'execTime.BA', 'execTime.LP', 'execTime.MP1',
                            'execTime.MP2', 'execTime.KALMAN',
                            'execTime.IML', 'execTime.EML',
                            'groundTruth.attack', 'groundTruth.attackType',
                            'groundTruth.threshold', 'groundTruth.repNum'
                            ]
                        ) for f in glob.glob("{}/*trace.csv".format(data_path))
                        ],
                ignore_index = True
                )
 
        # sort the data according to time
        df.sort_values(by=['simTime'], inplace=True)

        # process data into boolean for easier operations
        df['ratings.ewma.suspicionCopy'] =  df['ratings.ewma.suspicion']
        df['ratings.instant.suspicionCopy'] = df['ratings.instant.suspicion']
        # self.df['ratings.ewma.suspicion'] = \
                # np.where(self.df['ratings.ewma.suspicion'] >
                        # self.df['groundTruth.threshold'], True, False)

        # fill NaN values with 0
        # self.df.timeLatency = np.nan_to_num(self.df.timeLatency)
        # self.df.bsmLatency = np.nan_to_num(self.df.bsmLatency)
        # self.df.distanceLatency = np.nan_to_num(self.df.distanceLatency)
        return df

    def extract_vehicle_density_data(self, data_path):
        vdf = cu.sanitize_dataframe(
                pd.concat(
                    [
                        pd.read_csv(
                            f, names = [
                                "simTime",
                                "vehicleDensity",
                                "repNum"
                                ]
                            ) for f in glob.glob(
                                "{}/*_vehicle_density.csv".format(data_path)
                                )
                            ]
                    )
                )
        vdf.sort_values(by=['simTime'], inplace=True)
        vdf.fillna(0.0, inplace=True)
        # self.vehicle_density_data["genuineTypeCount"] = (
                # self.vehicle_density_data['vehicleType'] == "Genuine"
                # ).cumsum()
        # self.vehicle_density_data["attackerTypeCount"] = (
                # self.vehicle_density_data['vehicleType'] == "Attacker"
                # ).cumsum()
        return vdf
