import core.check as cc

class Vehicle:
    def __init__(self, v_id, omnetpp_path):
        self.id = v_id
        self.isGenuine = True
        self.omnetpp_path = omnetpp_path
        self.kalman = cc.Check()
        self.art = cc.Check()
        self.saw = cc.Check()
        self.mbf = cc.Check()
        self.mvp = cc.Check()
        self.mvn = cc.Check()
        self.map = cc.Check()
        self.man = cc.Check()
        self.mdm = cc.Check()
        self.fbr = cc.Check()
        self.eeblMpsOff = cc.Check()
        self.eeblMpsOn = cc.Check()
        self.suspicion = cc.Check()
        self.mlPrediction = cc.Check()
        self.rv_list = []
        self._read_omnetpp_dot_ini()



    def populateRvs(self, data):
        rv_ids = data.rvId.unique()
        self.rv_list = [Vehicle(rv_id, self.omnetpp_path) for rv_id in rv_ids]

        for rv in self.rv_list:
            rv_latest_ratings = (data.loc[data.rvId == rv.id]).tail(1)
            rv.art.value = rv_latest_ratings['ratings.ewma.ART'].item()
            rv.mbf.value = rv_latest_ratings['ratings.ewma.MBF'].item()
            rv.saw.value = rv_latest_ratings['ratings.ewma.SAW'].item()
            rv.mvp.value = rv_latest_ratings['ratings.ewma.MVP'].item()
            rv.mvn.value = rv_latest_ratings['ratings.ewma.MVN'].item()
            rv.map.value = rv_latest_ratings['ratings.ewma.MAP'].item()
            rv.man.value = rv_latest_ratings['ratings.ewma.MAN'].item()
            rv.mdm.value = rv_latest_ratings['ratings.ewma.MDM'].item()
            rv.fbr.value = rv_latest_ratings['ratings.ewma.FBR'].item()
            rv.kalman.value = rv_latest_ratings['ratings.ewma.KALMAN'].item()
            rv.suspicion.value = rv_latest_ratings['ratings.ewma.suspicionCopy'].item()
            rv.mlPrediction.value = rv_latest_ratings['ewma.mlPrediction'].item()
            rv.eeblMpsOff.value = rv_latest_ratings['eebl.mpsOff'].item()
            rv.eeblMpsOn.value = rv_latest_ratings['eebl.mpsOn'].item()

            rv.kalman.update_color()
            rv.art.update_color()
            rv.saw.update_color()
            rv.mbf.update_color()
            rv.mvp.update_color()
            rv.mvn.update_color()
            rv.map.update_color()
            rv.man.update_color()
            rv.mdm.update_color()
            rv.fbr.update_color()

            th = rv_latest_ratings['groundTruth.threshold'].item()
            rv.suspicion.update_color(orange = 0.5 * th, red = th)

            rv.isGenuine = rv_latest_ratings['groundTruth.attackType'].item() == "Genuine"
        return self.rv_list



    def is_enabled(self, line):
        return True if int(line.split()[-1]) == 1 else False



    def _read_omnetpp_dot_ini(self):
        lines = []
        self.suspicion.is_enabled = True
        with open(self.omnetpp_path, 'r') as omnet_fh:
            lines = omnet_fh.readlines()

        for line in lines:
            if "kalmanGain" in line:
                self.kalman.is_enabled = self.is_enabled(line)
                continue

            elif "artGain" in line:
                self.art.is_enabled = self.is_enabled(line)
                continue

            elif "sawGain" in line:
                self.saw.is_enabled = self.is_enabled(line)
                continue

            elif "maVelPosGain" in line:
                self.mvp.is_enabled = self.is_enabled(line)
                continue

            elif "maVelNegGain" in line:
                self.mvn.is_enabled = self.is_enabled(line)
                continue

            elif "mbfGain" in line:
                self.mbf.is_enabled = self.is_enabled(line)
                continue

            elif "maAccelPosGain" in line:
                self.map.is_enabled = self.is_enabled(line)
                continue

            elif "maAccelNegGain" in line:
                self.man.is_enabled = self.is_enabled(line)
                continue

            elif "fbrGain" in line:
                self.fbr.is_enabled = self.is_enabled(line)
                continue

            elif "mdmThreshMultiplier" in line:
                self.mdm.is_enabled = self.is_enabled(line)
                continue