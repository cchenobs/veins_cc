#! /bin/bash

printUsage() {
  echo "usage: $0 <sudden | suddend | constant | constantd | commrange | commranged | noattack | randposd | dos | fakeEebl> <log_path>"
  exit 1
}

if [[ $# -lt 2 ]]; then
  printUsage
fi

# select configuration according to command line input
config=""
case "$1" in
  "noattack")
     config="NoAttacks"
     ;;
  "sudden")
     config="SuddenAppearance"
     ;;
  "suddend")
     config="SuddenAppearanceDetection"
     ;;
  "constant")
     config="ConstantAttacker"
     ;;
  "constantd")
     config="ConstantAttackerDetection"
     ;;
  "commrange")
     config="CommRangeBraking"
     ;;
  "commranged")
     config="CommRangeBrakingDetection"
     ;;
  "randposd")
     config="RandomPositionDetection"
     ;;
   "dos")
     config="DoS"
     ;;
   "fakeEebl")
     config="FakeEEBL"
     ;;
  *)
     printUsage
     ;;
 esac

log_path=$2

# move to folder from where the simulation is to be run
cd $HOME/src/veins/examples/mps

# clean up old data files
if [ -d results/$log_path ]; then
  rm -rf results/$log_path
fi

echo "[General]
**.appl.logPath = \"$log_path\"" > log_path.ini

# run simulation
opp_run -m -u Cmdenv -c $config -n .:../veins:../../src/veins --image-path=../../images -l ../../src/veins omnetpp.ini log_path.ini &

while [ ! -f results/$log_path/*_trace.csv ]; do
  sleep 2
  continue
done

#while [ ! -f results/$(date +"%Y%m%d_%H")/trace_data/*traceData.json ]; do
  #continue
#done

#scripts/visualize.py --animate results/$log_path
#scripts/eebl.py --animate results/$log_path &
#scripts/gui.py

# clean up
rm -rf log_path.ini

# prettify JSON logs
#cat 8_senderRatings.json | ~/src/veins/rapidjson-bin/pretty > tmp.json
#cat tmp.json > 8_senderRatings.json

#cat 8_traceData.json | ~/src/veins/rapidjson-bin/pretty > tmp.json
#cat tmp.json > 8_traceData.json

#cat 14_groundTruthData.json | ~/src/veins/rapidjson-bin/pretty > tmp.json
#cat tmp.json > 14_groundTruthData.json

#cat 8_traceData.json
#cat 8_senderRatings.json
#cat 14_groundTruthData.json
