import core
import glob

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# get all types of results
# folders = glob.glob("/home/mps/Downloads/Results_20190502/*")
folders = [ "/home/mps/Downloads/Results_20190423/CommRangeBraking" ]
fig = {}
ax = {}
checks = ["ART", "MBF", "SAW", "MVP", "MVN", "MAP", "MAN", "MDM", "KALMAN",
        "PSA", "SA", "PO", "MDT", "BA", "LP", "MP1", "MP2", "FBR", "suspicion"]
attacks = [attack.split("/")[-1] for attack in folders]
thresholds = np.arange(0.0, 1.2, 0.2)

c = core.Core()
for folder in folders:
    c.extract_data(folder)
    for threshold in thresholds:
        e_checks = c.get_ewma_checks(threshold)
        for check in e_checks:
            print("attack: {}, threshold: {}, check: {}, TPR: {}".format(
                folder.split("/")[-1], threshold, check.name.split(".")[-1], check.tpr[-1]))
    # fig, ax = plt.sublplots(figsize=(10, 7.5))
    # ax.plot(thresholds, art.tpr)
    # ax.plot(thresholds, art.fpr)