############################################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Plots for MPS Check execution time
############################################################

import numpy as np

import core
from plots.core import *

class ExecTimePlot(CustomFigCanvas):
    def __init__(self):
        super(ExecTimePlot, self).__init__()

        self.core = core.Core()
        self.figure.suptitle("MPS Check Execution Time")

        self.plots = {}
        self.setup_figure()
        self.figure.subplots_adjust(top=0.945, bottom = 0.060, left = 0.065,
                right = 0.985, hspace = 0.330, wspace = 0.200)



    def setup_figure(self):
        self.plots["scatter_plots"] = self.figure.add_subplot(121)
        self.plots["scatter_plots"].set_xlabel("Sim-Time (ms)")
        self.plots["scatter_plots"].set_ylabel("Exec Time (ns)")

        self.plots["box_plots"] = self.figure.add_subplot(122)
        self.plots["box_plots"].set_ylabel("Exec Time (ns)")

    def clear_figure(self):
        for plot in self.plots:
            self.plots[plot].clear()

    def plot(self):
        self.clear_figure()
        self.setup_figure()

        time, exec_times = self.core.get_exec_times()
        exec_times["execTime.Total"] = exec_times.sum(axis = 1)

        # get names of axes from dataframe
        labels = [exec_time.split(".")[-1] for exec_time in exec_times.columns]
        for exec_time in exec_times.columns:
            label_ = exec_time.split(".")[-1]
            self.plots["scatter_plots"].scatter(time, exec_times[exec_time],
                    label = label_, marker = ".", alpha = 0.4)

        # Plot box plots of checks execution time
        # must convert exec_times to list of lists for boxplot
        box_exec_times = exec_times.values.T.tolist()
        self.plots["box_plots"].boxplot(box_exec_times, sym='')
        self.plots["box_plots"].set_xticklabels(labels, rotation=45)

        self.print_legend()
        self.draw_idle()

    def print_legend(self):
        self.plots["scatter_plots"].legend(loc='lower right')

