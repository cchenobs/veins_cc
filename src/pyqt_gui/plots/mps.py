############################################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Plots metrics to evaluate MPS performance
############################################################

import numpy as np
from core.utils import calculate_metrics, sanitize_dataframe

from plots.performance import *

class MpsPlot(PerformancePlotBase):
    def __init__(self):
        super(MpsPlot, self).__init__()
        self.figure.suptitle("MPS Performance")
        self.linewidth = 1.0
        self.n = None
        self.entities = None

    def setup_figure(self):
        super(MpsPlot, self).setup_figure()
        self.entities = self.core.get_ewma_checks()
        plot_keys = [entity.name.split(".")[-1] for entity in self.entities]
        self.plots["base_confusion"].set_xticklabels(plot_keys, rotation=40)
        self.n = np.arange(len(self.entities))
        self.plots["base_confusion"].set_xticks(self.n + self.bar_width)

    def plot(self):
        self.clear_figure()
        self.setup_figure()

        tps = [entity.tp for entity in self.entities]
        fps = [entity.fp for entity in self.entities]
        tns = [entity.tn for entity in self.entities]
        fns = [entity.fn for entity in self.entities]

        self.plots["base_confusion"].bar(self.n,                      tps, self.bar_width, label='TP')
        self.plots["base_confusion"].bar(self.n + self.bar_width,     fps, self.bar_width, label='FP')
        self.plots["base_confusion"].bar(self.n + self.bar_width * 2, fns, self.bar_width, label='FN')
        self.plots["base_confusion"].bar(self.n + self.bar_width * 3, tns, self.bar_width, label='TN')

        # plot vehicle density
        time, vd = self.core.get_vehicle_density()
        self.plots["vehicle_density"].plot(time, vd)

        # plot checks
        time = self.core.get_time_series()
        for e in self.entities:
            opacity = self.opacity
            lw = self.linewidth

            label_ = e.name.split(".")[-1]
            # bold_lines = ('SUSP', 'EML', 'IML')
            bold_lines = ("ART")
            if label_ in bold_lines:
                opacity = 1.0
                lw = 3.0

            self.plots["accuracy" ].plot(time, e.accuracy,  label=label_, alpha=opacity, linewidth = lw)
            self.plots["precision"].plot(time, e.precision, label=label_, alpha=opacity, linewidth = lw)
            self.plots["recall"   ].plot(time, e.recall,    label=label_, alpha=opacity, linewidth = lw)
            self.plots["f1_score" ].plot(time, e.f1,        label=label_, alpha=opacity, linewidth = lw)

        self.print_legend()
        self.draw_idle()

# class CustomMainWindow(QtGui.QMainWindow):

    # def __init__(self):

        # super(CustomMainWindow, self).__init__()

        # # Define the geometry of the main window
        # # self.setGeometry(300, 300, 800, 400)
        # self.setWindowTitle("MPS Performance")

        # # Create central widget
        # centralWidget = QtGui.QWidget(self)
        # layout = QtGui.QGridLayout(centralWidget)
        # centralWidget.setLayout(layout)
        # self.setCentralWidget(centralWidget)

        # # Place the matplotlib figure
        # mpsPlot = MpsPlot()
        # mpsPlot.log_path = "/home/mps/src/veins/examples/mps/results/fakeEEBLSuccess_md_4.0/"
        # toolbar = NavigationToolbar(mpsPlot, self)
        # button = QtGui.QPushButton('Plot')
        # aniButton = QtGui.QPushButton('Animate')
        # button.clicked.connect(mpsPlot.plotData)
        # aniButton.clicked.connect(mpsPlot.animatePlot)

        # layout.addWidget(mpsPlot)
        # layout.addWidget(toolbar)
        # layout.addWidget(button)
        # layout.addWidget(aniButton)

        # self.show()



# if __name__ == '__main__':
    # import sys
    # app = QtGui.QApplication(sys.argv)
    # CustomMainWindow()
    # sys.exit(app.exec_())


