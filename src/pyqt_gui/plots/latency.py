############################################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Plots for MPS latencies
############################################################

import core
from plots.core import *
from matplotlib.ticker import LinearLocator

class LatencyPlot(CustomFigCanvas):
    def __init__(self):
        super(LatencyPlot, self).__init__()

        self.core = core.Core()
        self.figure.suptitle("MPS Latencies")

        self.plots = {}
        self.setup_figure()
        self.figure.subplots_adjust(top=0.945, bottom = 0.060, left = 0.065,
                right = 0.985, hspace = 0.330, wspace = 0.240)

    def clear_figure(self):
        self.plots["sim_real"].clear()
        self.plots["sim_bsm"].clear()
        self.plots["sim_dist"].clear()
        self.plots["sim_sim"].clear()
        self.plots["real_bsm"].clear()
        self.plots["real_dist"].clear()
        self.plots["bsm_dist"].clear()
        # self.plots["ram_use"].clear()
        # self.plots["cpu_use"].clear()

    def setup_figure(self):
        sim_time_label = 'Sim-Time (s)'
        sim_time_latency_label = 'Sim-Time Latency (s)'
        real_time_label = 'Real-time (ms)'
        bsm_label = '# BSMs'
        distance_label = 'Distance (m)'

        self.plots["sim_real"]  = self.figure.add_subplot(431)
        self.plots["sim_bsm"]   = self.figure.add_subplot(432)
        self.plots["sim_dist"]  = self.figure.add_subplot(433)
        self.plots["sim_sim"]   = self.figure.add_subplot(434)
        self.plots["real_bsm"]  = self.figure.add_subplot(435)
        self.plots["real_dist"] = self.figure.add_subplot(436)
        self.plots["bsm_dist"]  = self.figure.add_subplot(437)
        # self.plots["ram_use"]   = self.figure.add_subplot(438)
        # self.plots["cpu_use"]   = self.figure.add_subplot(439)

        self.plots["sim_real"].set_xlabel(sim_time_label)
        self.plots["sim_real"].set_ylabel(real_time_label)

        self.plots["sim_bsm"].set_xlabel(sim_time_label)
        self.plots["sim_bsm"].set_ylabel(bsm_label)

        self.plots["sim_dist"].set_xlabel(sim_time_label)
        self.plots["sim_dist"].set_ylabel(distance_label)

        self.plots["sim_sim"].set_xlabel(sim_time_label)
        self.plots["sim_sim"].set_ylabel(sim_time_latency_label)

        self.plots["real_bsm"].set_xlabel(real_time_label)
        self.plots["real_bsm"].set_ylabel(bsm_label)

        self.plots["real_dist"].set_xlabel(real_time_label)
        self.plots["real_dist"].set_ylabel(distance_label)

        self.plots["bsm_dist"].set_xlabel(bsm_label)
        self.plots["bsm_dist"].set_ylabel(distance_label)

        # self.plots["ram_use"].set_xlabel("Time (s)")
        # self.plots["ram_use"].set_ylabel("Memory Use (%)")

        # self.plots["cpu_use"].set_xlabel("Time (s)")
        # self.plots["cpu_use"].set_ylabel("CPU Use (%)")

    def plot(self):
        self.clear_figure()
        self.setup_figure()

        time, latencies = self.core.get_latencies()

        # convert real-time latency to realtime + (nBSMs - 1) * 1/f
        realtime = latencies['latency.realTime'] + (latencies['latency.bsm'] - 1) * 1 / 10

        # read RAM usage file
        # systemResourceUsage = pd.read_csv(self.log_path + "/ramUsage.csv", names=['ram', 'cpu'])

        opacity = 0.4
        self.plots["sim_real"].scatter (time, realtime,                          c='r',      marker='.', alpha=opacity)
        self.plots["sim_bsm"].scatter  (time, latencies['latency.bsm'],          c='g',      marker='.', alpha=opacity)
        self.plots["sim_dist"].scatter (time, latencies['latency.distance'],     c='b',      marker='.', alpha=opacity)
        self.plots["sim_sim"].scatter  (time, latencies['latency.simTime'],      c='purple', marker='.', alpha=opacity)
        self.plots["real_bsm"].scatter (realtime, latencies['latency.bsm'],      c='y',      marker='.', alpha=opacity)
        self.plots["real_dist"].scatter(realtime, latencies['latency.distance'], c='teal',   marker='.', alpha=opacity)
        self.plots["bsm_dist"].scatter (latencies['latency.bsm'], realtime,      c='violet', marker='.', alpha=opacity)
        # self.plots["ram_use"].plot(systemResourceUsage.ram * 100,              c='pink')
        # self.plots["cpu_use"].plot(systemResourceUsage.cpu,                    c='gray')

        self.draw_idle()

