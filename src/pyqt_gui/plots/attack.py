############################################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Plots metrics to evaluate EEBL attack performance
############################################################

import numpy as np
from core.utils import calculate_metrics, sanitize_dataframe

from plots.performance import *

class AttackPlot(PerformancePlotBase):
    def __init__(self):
        super(AttackPlot, self).__init__()
        self.figure.suptitle("Attack Performance")

    def setup_figure(self):
        super(AttackPlot, self).setup_figure()
        self.plots["success_rate"] = self.figure.add_subplot(self.gs[2, 1])
        self.plots["success_rate"].set_title("Success Rate")
        self.plots["success_rate"].set_ylabel("TPR")
        self.plots["success_rate"].grid()

        self.attacks = self.core.get_attacks()
        plot_keys = [attack.name.split(".")[-1] for attack in self.attacks]
        self.plots["base_confusion"].set_xticklabels(plot_keys, rotation=40)
        self.n = np.arange(len(self.attacks))
        self.plots["base_confusion"].set_xticks(self.n + self.bar_width)

    def plot(self):
        self.clear_figure()
        self.setup_figure()

        # plot vehicle density
        time, vd = self.core.get_vehicle_density()
        self.plots["vehicle_density"].plot(time, vd)

        # plot checks
        tps = []
        fps = []
        tns = []
        fns = []
        for attack in self.attacks:
            tps.append(attack.tp)
            fps.append(attack.fp)
            tns.append(attack.tn)
            fns.append(attack.fn)

        self.plots["base_confusion"].bar(self.n,                      tps, self.bar_width, label='TP')
        self.plots["base_confusion"].bar(self.n + self.bar_width,     fps, self.bar_width, label='FP')
        self.plots["base_confusion"].bar(self.n + self.bar_width * 2, fns, self.bar_width, label='FN')
        self.plots["base_confusion"].bar(self.n + self.bar_width * 3, tns, self.bar_width, label='TN')

        time = self.core.get_time_series()
        for a in self.attacks:
            label_ = a.name.split(".")[-1]

            self.plots["accuracy"].plot    (time, a.accuracy,  label=label_)
            self.plots["precision"].plot   (time, a.precision, label=label_)
            self.plots["recall"].plot      (time, a.recall,    label=label_)
            self.plots["f1_score"].plot    (time, a.f1,        label=label_)
            self.plots["success_rate"].plot(time, a.tpr,       label=label_)

        self.print_legend()
        self.draw_idle()

def get_attack_df(data):
    attack_df = data[[
        'simTime','suspectedAttack','eebl.mpsOff','eebl.mpsOn'
        ]].copy()

    d, attack_df = calculate_metrics(attack_df.iloc[:, 1:], data['groundTruth.attackGt'])
    return (d, attack_df)

# class CustomMainWindow(QtGui.QMainWindow):

    # def __init__(self):

        # super(CustomMainWindow, self).__init__()

        # # Define the geometry of the main window
        # self.setGeometry(300, 300, 800, 400)
        # self.setWindowTitle("MPS Performance")

        # # Create central widget
        # self.centralWidget = QtGui.QWidget(self)
        # self.layout = QtGui.QGridLayout(self.centralWidget)
        # self.centralWidget.setLayout(self.layout)
        # self.setCentralWidget(self.centralWidget)

        # # Place the matplotlib figure
        # self.figure = CustomFigCanvas()
        # self.toolbar = NavigationToolbar(self.figure.canvas, self)
        # self.layout.addWidget(self.figure)
        # self.layout.addWidget(self.toolbar)

        # self.show()



# if __name__ == '__main__':
    # import sys
    # app = QtGui.QApplication(sys.argv)
    # gui = CustomMainWindow()

    # sys.exit(app.exec_())

