############################################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Core plotting class for MPS performance
############################################################

from abc import abstractmethod

from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
import matplotlib.animation as animation
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSlot, pyqtSignal
import core

# from threads.data import DataThread
from core.dataextractor import DataExtractor

class CustomFigCanvas(FigureCanvas, QtCore.QObject):

    def __init__(self):
        QtCore.QObject.__init__(self)
        self.figure = Figure()
        self.gs = GridSpec(3, 3, figure = self.figure)
        super(CustomFigCanvas, self).__init__(self.figure)
        self.log_path = ""
        self.core = core.Core()
        self._animate_timer = QtCore.QTimer()
        self.figure.subplots_adjust(top=0.925, bottom = 0.060, left = 0.050,
                right = 0.990, hspace = 0.360, wspace = 0.320)
        # self.dataThread = DataThread()
        # self.dataThread.data_ready.connect(self.refresh)

    @abstractmethod
    def setup_figure(self):
        raise NotImplementedError

    @abstractmethod
    def clear_figure(self):
        raise NotImplementedError

    def animatePlot(self):
        print("animating plot")
        self._animate_timer.start(1000)
        self._animate_timer.timeout.connect(self.plotData)

    @abstractmethod
    def plot(self):
        raise NotImplementedError

    @abstractmethod
    def print_legend(self):
        raise NotImplementedError

    # @abstractmethod
    # @pyqtSlot(Data)
    # def refresh(self, data):
    #     raise NotImplementedError

    # @abstractmethod
    # @pyqtSlot(Data)
    # def set_data(self, data):
    #     raise NotImplementedError

