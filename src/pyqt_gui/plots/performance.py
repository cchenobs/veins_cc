############################################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Core plotting class for MPS performance
############################################################

from plots.core import *

class PerformancePlotBase(CustomFigCanvas):

    attack_data_ready = pyqtSignal(int, int, int)
    def __init__(self):
        super(PerformancePlotBase, self).__init__()
        self.bar_width = 0.15
        self.opacity = 0.5

        self.plots = {}

        self.setup_figure()

        # self.figure.subplots_adjust(top=0.915, bottom = 0.075, left = 0.035,
                # right = 0.905, hspace = 0.250, wspace = 0.400)

    def setup_figure(self):
        limits = [-0.01, 1.01]

        #create subplots
        self.plots["base_confusion"]  = self.figure.add_subplot(self.gs[0, :-1])
        self.plots["vehicle_density"] = self.figure.add_subplot(self.gs[0, -1])
        self.plots["precision"]       = self.figure.add_subplot(self.gs[1, 0])
        self.plots["recall"]          = self.figure.add_subplot(self.gs[1, 1])
        self.plots["f1_score"]        = self.figure.add_subplot(self.gs[1, 2])
        self.plots["accuracy"]        = self.figure.add_subplot(self.gs[2, 0])

        # set subplot titles
        self.plots["base_confusion"].set_title("Confusion")
        self.plots["accuracy"].set_title("Accuracy during simulation")
        self.plots["vehicle_density"].set_title("Vehicle density in simulation")
        self.plots["precision"].set_title("Precision during simulation")
        self.plots["recall"].set_title("Recall during simulation")
        self.plots["f1_score"].set_title("F1 score during simulation")

        # axes settings
        self.plots["base_confusion"].set_ylabel("Count")

        self.plots["accuracy"].set_xlabel("Time")
        self.plots["accuracy"].set_ylabel("Accuracy")
        self.plots["accuracy"].set_ylim(limits)
        self.plots["accuracy"].grid()

        self.plots["vehicle_density"].set_xlabel("Time")
        self.plots["vehicle_density"].set_ylabel("No. of Vehicles")
        self.plots["vehicle_density"].grid()

        self.plots["precision"].set_xlabel("Time")
        self.plots["precision"].set_ylabel("Precision")
        self.plots["precision"].set_ylim(limits)
        self.plots["precision"].grid()

        self.plots["recall"].set_xlabel("Time")
        self.plots["recall"].set_ylabel("Recall")
        self.plots["recall"].set_ylim(limits)
        self.plots["recall"].grid()

        self.plots["f1_score"].set_xlabel("Time")
        self.plots["f1_score"].set_ylabel("F1 score")
        self.plots["f1_score"].set_ylim(limits)
        self.plots["f1_score"].grid()

    def clear_figure(self):
        for plot in self.plots:
            self.plots[plot].clear()

    def print_legend(self):
        self.plots["base_confusion"].legend(loc='upper right', ncol = 2)

        h, l = self.plots["accuracy"].get_legend_handles_labels()
        self.figure.legend(h, l, loc = 'lower right', ncol=2)

    # @abstractmethod
    # @pyqtSlot(Data)
    # def refresh(self, data):
    #     raise NotImplementedError

    # @abstractmethod
    # @pyqtSlot(Data)
    # def set_data(self, data):
    #     raise NotImplementedError

