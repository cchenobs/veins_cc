import core
import glob

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# get all types of results
folders = glob.glob("/home/mps/Downloads/Results_20190502/*")
# folders = [ "/home/mps/Downloads/Results_20190423/CommRangeBraking" ]
bar_width = 0.1
fig = {}
ax = {}
checks = ["ART", "MBF", "SAW", "MVP", "MVN", "MAP", "MAN", "MDM", "KALMAN",
        "PSA", "SA", "PO", "MDT", "BA", "LP", "MP1", "MP2", "FBR", "suspicion"]

attacks = [attack.split("/")[-1] for attack in folders]

tpr_df = pd.DataFrame(index = attacks, columns = checks)
fpr_df = pd.DataFrame(index = attacks, columns = checks)
p_df = pd.DataFrame(index = attacks, columns = checks)
r_df = pd.DataFrame(index = attacks, columns = checks)
f1_df = pd.DataFrame(index = attacks, columns = checks)
acc_df = pd.DataFrame(index = attacks, columns = checks)

core = core.Core()
for folder in folders:
    attack = folder.split("/")[-1]
    print("processing attack {}".format(attack))
    core.extract_data(folder)

    # get data
    e_checks = core.get_ewma_checks()
    i_checks = core.get_instant_checks()

    #create dataframe for plotting
    df = {}
    rows = [check.name.split(".")[-1] for check in e_checks]
    cols = ("TPR","FPR","Precision","Recall","F1","Accuracy")
    df["e"] = pd.DataFrame(index=rows, columns=cols)

    # store data in dataframe
    for check in e_checks:
        check_name = check.name.split(".")[-1]

        tpr = check.tpr[-1]
        fpr = check.fpr[-1]
        p = check.precision[-1]
        r = check.recall[-1]
        f1 = check.f1[-1]
        acc = check.accuracy[-1]

        df["e"].loc[check_name] = (tpr, fpr, p, r, f1, acc)

        tpr_df.loc[attack, check_name] = tpr
        fpr_df.loc[attack, check_name] = fpr
        p_df.loc[attack, check_name] = p
        r_df.loc[attack, check_name] = r
        f1_df.loc[attack, check_name] = f1
        acc_df.loc[attack, check_name] = acc

    # plot data
    fig[attack], ax[attack] = plt.subplots(figsize=(15.0,7.5))
    ax[attack].set_title("Plausiblity detector performance against {}".format(attack))
    ax[attack].set_xticklabels(rows, rotation=40, fontsize="small")
    n = np.arange(len(rows))
    ax[attack].set_xticks(n + bar_width)
    for i, col in enumerate(cols):
        ax[attack].bar(n + bar_width * i, df["e"][col], bar_width, label=col)

    fig[attack].legend(loc="upper right", bbox_to_anchor=(1.0,0.9), fontsize="small")
    # fig[attack].tight_layout()
    print("saving figure for {}".format(attack))
    fig[attack].savefig("e_attack_{}.svg".format(attack), format="svg")

##############################################################################

sfig = {}
sax = {}
rows = [ "TPR","FPR","Precision","Recall","F1","Accuracy" ]

for row in rows:
    print("processing graph {}".format(row))
    sfig[row], sax[row] = plt.subplots(figsize=(15.0,7.5))
    sax[row].set_title("Plausiblity detector {} against attacks".format(row))
    sax[row].set_xticklabels(attacks, fontsize="small")
    n = np.arange(len(attacks))
    sax[row].set_xticks(n + bar_width)

for i, check in enumerate(checks):
    sax[rows[0]].bar(n + bar_width * i, tpr_df[check], bar_width, label=check)
    sax[rows[1]].bar(n + bar_width * i, fpr_df[check], bar_width, label=check)
    sax[rows[2]].bar(n + bar_width * i, p_df[check], bar_width, label=check)
    sax[rows[3]].bar(n + bar_width * i, r_df[check], bar_width, label=check)
    sax[rows[4]].bar(n + bar_width * i, f1_df[check], bar_width, label=check)
    sax[rows[5]].bar(n + bar_width * i, acc_df[check], bar_width, label=check)

for row in rows:
    sfig[row].legend(loc="upper right", bbox_to_anchor=(1.0,0.9), fontsize="small")
    # sfig[row].tight_layout()
    print("saving figure for {}".format(row))
    sfig[row].savefig("e_metric_{}.svg".format(row), format="svg")
print("done!")
# plt.show()
