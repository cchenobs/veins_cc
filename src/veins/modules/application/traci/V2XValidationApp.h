//
// Copyright (C) 2016 David Eckhoff <david.eckhoff@fau.de>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef __VEINS_V2XVALIDATIONAPP_H_
#define __VEINS_V2XVALIDATIONAPP_H_

#include <omnetpp.h>
#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/mobility/traci/TraCIConnection.h"

using namespace omnetpp;

/**
 * @brief
 * This is a stub for a typical Veins application layer.
 * Most common functions are overloaded.
 * See V2XValidationApp.cc for hints
 *
 * @author David Eckhoff
 *
 */

class V2XValidationApp : public BaseWaveApplLayer {
public:
  virtual void initialize(int stage) override;
  virtual void finish() override;
protected:
  virtual void onBSM(BasicSafetyMessage* bsm) override;
  virtual void onWSM(WaveShortMessage* wsm) override;
  virtual void onWSA(WaveServiceAdvertisment* wsa) override;

  virtual void handleSelfMsg(cMessage* msg) override;
  virtual void handlePositionUpdate(cObject* obj) override;
private:
  bool brake_;
//  Veins::TraCIConnection* connection_ = &traci->connection;
};

#endif
