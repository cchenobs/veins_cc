//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/SuddenAppearanceWarning.h>

SuddenAppearanceWarning::SuddenAppearanceWarning()
{}

SuddenAppearanceWarning::~SuddenAppearanceWarning()
{}

void SuddenAppearanceWarning::
execute(Sender& sender, const Coord& receiverPos, const double radiusOfInterest, const Coord& receiverHeading) {
//  if (!sender.isFirstBeacon())
//    return;

  // get Euclidean distance between BSM and receiver
  const auto distance = sender.getBsm().getSenderPos().distance(receiverPos);

  // For Standard Error (Confidence Interval) assume:
  // mean              = 300 meters
  // standardDeviation = 15 meters
  // CI                = 95%
  // standardError     = standardDeviation / squareRootOfSampleSize
  // Multiply standardError by 1.96 for 95% confidence interval
  // marginOfError = 1.96 * standardDeviation / squareRootOfSampleSize
  const auto MARGIN_OF_ERROR = 0.93;

//  auto isSenderBehind = SupportFunctions().isBehind(receiverPos, sender_.getPosition(), receiverHeading);

  if (distance <= radiusOfInterest + MARGIN_OF_ERROR) {// && !isSenderBehind) {
    if (sender.isFirstBeacon()) {
      sender.getInstantRating().setSaw(1);
    } else {
        if (sender.getEwmaRating().getSaw() > 0.0) {
          sender.getInstantRating().setSaw(1);
        }
    }
  }

  // update rating when SAW fails
//  updateSenderRating(SAW, -1);
}
