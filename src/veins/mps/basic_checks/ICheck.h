//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MPS_BASIC_CHECKS_ICHECK_H_
#define SRC_VEINS_MPS_BASIC_CHECKS_ICHECK_H_

#include <veins/mps/utility/Sender.h>
#include <veins/mps/utility/Latency.h>

class ICheck
{
public:
  ICheck();
  virtual ~ICheck();
  double getFadeFactor() const;
  int getGain() const;
  void setFadeFactor(double fadeFactor);
  void setGain(int gain);
  const Latency& getLatency() const;
  void setLatency(const Latency& latency);
  double execTime_;

protected:
  Latency latency_;
  double  fadeFactor_;
  int     gain_;
};

#endif /* SRC_VEINS_MPS_BASIC_CHECKS_ICHECK_H_ */
