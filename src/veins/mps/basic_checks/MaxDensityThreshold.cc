//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/MaxDensityThreshold.h>
#include <veins/mps/utility/SupportFunctions.h>

MaxDensityThreshold::MaxDensityThreshold()
{
  // TODO Auto-generated constructor stub

}

MaxDensityThreshold::~MaxDensityThreshold()
{
  // TODO Auto-generated destructor stub
}



void MaxDensityThreshold::
execute(const Coord& vehPos, Sender& sender, SenderTable& senderTable, double rectLen, double rectWidth, double vehLen, double vehWidth, const Coord& space) {
  // max_possible_vehicles = area_of_rectangle / area_of_single_vehicle
  const auto MAX_POSSIBLE_VEHICLES = static_cast<int>((rectLen * rectWidth) / ((vehLen + 2 * space.y) * (vehWidth + 2 * space.x)));

  // get vertices of the rectangle i.e. length m ahead and behind and width m wide
  auto support = SupportFunctions();
  auto rect = support.getVertices(vehPos, rectLen, rectWidth);

  // count the number of vehicles in this rectangle
  auto nVehiclesInRect = 0;
  for (auto& s : senderTable.getSenders()) {
    if (isPointInRectangle(rect, s.getBsm().getSenderPos()))
      nVehiclesInRect++;

    // if more than the threshold then don't accept new BSM in this area
    if (nVehiclesInRect >= MAX_POSSIBLE_VEHICLES) {
      sender.getInstantRating().setMdt(1);
      break;
    }
  }
}



double MaxDensityThreshold::
triangleArea(const Triangle& t) const {
    return std::abs((
        t.a.x * (t.b.y - t.c.y) +
        t.b.x * (t.c.y - t.a.y) +
        t.c.x * (t.a.y - t.b.y)) / 2.0);
}


// Rectangle represented by ABCD
bool MaxDensityThreshold::
isPointInRectangle(const Rectangle& r, const Coord& p) const {
    // Calculate area of rectangle ABCD
    auto Ar = triangleArea(Triangle{r.a, r.b, r.c}) +
              triangleArea(Triangle{r.a, r.d, r.c});

    // Calculate area of triangle PAB
    auto Apab = triangleArea(Triangle{p, r.a, r.b});

    // Calculate area of triangle PBC
    auto Apbc = triangleArea(Triangle{p, r.b, r.c});

    // Calculate area of triangle PCD
    auto Apcd = triangleArea(Triangle{p, r.c, r.d});

    // Calculate area of triangle PAD
    auto Apad = triangleArea(Triangle{p, r.a, r.d});

    // Check if sum of A1, A2, A3 and A4 is same as A
    return (Ar == Apab + Apbc + Apcd + Apad);
}


