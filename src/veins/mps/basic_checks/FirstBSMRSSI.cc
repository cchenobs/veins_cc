//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include <veins/mps/basic_checks/FirstBSMRSSI.h>

FirstBSMRSSI::
FirstBSMRSSI()
{}



FirstBSMRSSI::
~FirstBSMRSSI()
{}



void FirstBSMRSSI::
execute(Sender& sender, const Coord& receiverPos, const std::vector<Range>& confidenceInterval) {
  // Calculate the Euclidean distance between sender and receiver
  const auto distance = sender.getBsm().getSenderPos().distance(receiverPos);
  const auto MAX_DISTANCE_CONSIDERED = 400;
  const auto MIN_DISTANCE_CONSIDERED = 1;

  int index = (int)std::floor(distance);
  if(index < MIN_DISTANCE_CONSIDERED || index > MAX_DISTANCE_CONSIDERED) {
    sender.getInstantRating().setFbr(1);
    sender.setFbrTrace(1);
    return;
  }

  auto currentU = confidenceInterval[index - 3].high;
  auto currentL = confidenceInterval[index - 3].low;
  auto senderRSSI = sender.getRssi();
  if (senderRSSI > currentU || senderRSSI < currentL) {
    sender.getInstantRating().setFbr(1);
    sender.setFbrTrace(1);
  }
}
