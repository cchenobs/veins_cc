//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/MovementPlausibility.h>

MovementPlausibility::MovementPlausibility()
{
  // TODO Auto-generated constructor stub

}

MovementPlausibility::~MovementPlausibility()
{
  // TODO Auto-generated destructor stub
}

void MovementPlausibility::
execute(Sender& sender) {
  auto bsm = sender.getBsm();
  auto pos = bsm.getSenderPos();
  auto spd = bsm.getSenderSpeed();
  auto timeStep = simTime().dbl() - sender.getLastBeaconTime();

  sender.setTotalDisplacement(pos - sender.getFirstPos());
  sender.setTotalDistanceVelocity(spd * timeStep);

  auto predictedVelocity = Coord();
  auto averageVelocity = Coord();
  auto totalDisplacement = sender.getTotalDisplacement();
  auto totalDistanceVelocity = sender.getTotalDistanceVelocity();
  auto totalTime = simTime().dbl() - sender.getFirstBeaconTime();

  predictedVelocity.x = totalDisplacement.x / totalTime;
  predictedVelocity.y = totalDisplacement.y / totalTime;
  averageVelocity.x = totalDistanceVelocity.x / totalTime;
  averageVelocity.y = totalDistanceVelocity.y / totalTime;

  auto velDiff = averageVelocity - predictedVelocity;

  sender.getInstantRating().setMp1(
      (totalDisplacement.x == 0 && averageVelocity.x != 0) ||
      (totalDisplacement.y == 0 && averageVelocity.y != 0));

  // Metric 3
  sender.getInstantRating().setMp2(velDiff.length() > 0);
}
