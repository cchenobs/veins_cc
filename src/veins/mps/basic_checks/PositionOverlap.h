//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MPS_BASIC_CHECKS_POSITIONOVERLAP_H_
#define SRC_VEINS_MPS_BASIC_CHECKS_POSITIONOVERLAP_H_

#include <veins/mps/basic_checks/ICheck.h>
#include <veins/mps/utility/SenderTable.h>
#include <veins/mps/utility/TypeDefs.h>

class PositionOverlap : public ICheck
{
public:
  PositionOverlap();
  virtual ~PositionOverlap();
  void execute(Sender& sender, SenderTable& senderTable);
  // Returns true if two rectangles (l1, r1) and (l2, r2) overlap
  bool isOverlapping(const Coord& l1, const Coord& r1, const Coord& l2, const Coord& r2) const;
};

#endif /* SRC_VEINS_MPS_BASIC_CHECKS_POSITIONOVERLAP_H_ */
