//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/MaxAcceleration.h>

MaxAcceleration::MaxAcceleration() {}

MaxAcceleration::~MaxAcceleration()
{}

void MaxAcceleration::
execute(Sender& sender) {
  // Maximum possible acceleration is being calculated by taking
  // 95% minimum confidence interval value from the values
  // obtained from this link:
  // https://en.wikipedia.org/wiki/List_of_fastest_production_cars_by_acceleration#By_0%E2%80%93100_km/h_time_or_0%E2%80%9360_mph_(0%E2%80%9397_km/h)_(3.0_seconds_or_less)[i]_[ii]
  const auto MAX_POSSIBLE_ACCELERATION = 36.96; //kmph^2

  auto senderAcceleration    = sender.getBsm().getSenderAcceleration().length();

  if (senderAcceleration > MAX_POSSIBLE_ACCELERATION)
    sender.getInstantRating().setMan(1);
//    updateSenderRating(MA_ACCEL_NEG, -1);
//  else
//    sender_.setMaAccelNegInstantRating(0);
//    updateSenderRating(MA_ACCEL_NEG, 0);
}
