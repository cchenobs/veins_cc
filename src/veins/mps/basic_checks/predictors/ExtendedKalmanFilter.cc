//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "veins/mps/basic_checks/predictors/ExtendedKalmanFilter.h"

ExtendedKalmanFilter::
ExtendedKalmanFilter (
    const double       timeStep,
    const Parameters&  params,
    const int          stateVectorSize,
    const int          controlVectorSize,
    const int          measurementVectorSize)
: params_(params)
{
  setTimeStep             (timeStep);
  setStateVectorSize      (stateVectorSize);
  setControlVectorSize    (controlVectorSize);
  setMeasurementVectorSize(measurementVectorSize);

  setA();
  setB();
  setH();
  setR();
  setQ();
  setP();
  setW();

  // Initial states
  auto x0 = Eigen::VectorXd(getStateVectorSize());
  x0 << params_.getPosition().x, params_.getPosition().y, params_.getSpeed().x, params_.getSpeed().y;
  setX(x0);

  setI();
  setInitialized(true);
}



ExtendedKalmanFilter::
~ExtendedKalmanFilter() {}



void ExtendedKalmanFilter::
setA() {
  auto dt = getTimeStep();

  // State transition Matrix
  A_ = Eigen::MatrixXd(getStateVectorSize(), getStateVectorSize()); // System dynamics matrix
  A_ << 1, 0, dt, 0,
        0, 1, 0,  dt,
        0, 0, 1,  0,
        0, 0, 0,  1;
}



void ExtendedKalmanFilter::
setB() {
  auto dt = getTimeStep();

  // Input Control Matrix (B_ = Eigen::MatrixXd::Identity(rows=n_states, cols=n_ctrl_states))
  B_ = Eigen::MatrixXd(getStateVectorSize(), getControlVectorSize()); // Control dynamics matrix
  B_ << 0.5 * dt * dt, 0,
        0            , 0.5 * dt * dt,
        dt           , 0,
        0            , dt;
}



void ExtendedKalmanFilter::
setH() {
  // Measurement Output Matrix
//    H_ = Eigen::MatrixXd(m, n); // Output matrix
//    H << 1, 0, 0, 0,
//         0, 1, 0, 0,
//         0, 0, 1, 0,
//         0, 0, 0, 1;
  H_ = Eigen::MatrixXd::Identity(getMeasurementVectorSize(), getStateVectorSize());
}



void ExtendedKalmanFilter::
setQ() {
  // Action Uncertainty
//    Q_ = Eigen::MatrixXd(n, n); // Process noise covariance
//    Q_ << 0.00001, 0,       0,       0,
//         0,       0.00001, 0,       0,
//         0,       0,       0.00001, 0,
//         0,       0,       0,       0.00001;
  Q_ = Eigen::MatrixXd::Identity(getStateVectorSize(), getStateVectorSize()) * 0.00001;
}



void ExtendedKalmanFilter::
setR() {
  // Measurement Noise (R_ = Eigen::MatrixXd::Identity(n_states, n_ctrl_states) * 0.1)
//    R_ = Eigen::MatrixXd(m, m); // Measurement noise covariance
//    R << 0.1, 0  , 0  , 0,
//         0  , 0.1, 0  , 0,
//         0  , 0  , 0.1, 0,
//         0  , 0  , 0  , 0.1;
  R_ = Eigen::MatrixXd::Identity(getMeasurementVectorSize(), getMeasurementVectorSize()) * 0.1;
}



void ExtendedKalmanFilter::
setP() {
  // Prediction Error (P_ = Eigen::MatrixXd::Identity(n_states, n_states) * 0.25)
//    P_ = Eigen::MatrixXd(n, n); // Estimate error covariance
//    P << 0.25, 0   , 0   , 0,
//         0   , 0.25, 0   , 0,
//         0   , 0   , 0.25, 0,
//         0   , 0   , 0   , 0.25;
  P_ = Eigen::MatrixXd::Identity(getStateVectorSize(), getStateVectorSize()) * 0.0001;
}



void ExtendedKalmanFilter::
setW() {
  // Prediction Noise (W_ = Eigen::VectorXd::Constant(n_states, 0) -OR- W_.setZero())
  W_ = Eigen::VectorXd::Constant(getStateVectorSize(), 0);
//    W << 0,
//         0,
//         0,
//         0;
//    W_ = Eigen::VectorXd(n); // Predicted state noise matrix
}
