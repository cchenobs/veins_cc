//-----------------------------------------------------------
// @author Raashid Ansari
// @date   06-29-2018
// @brief  This class implements the model used by Kalman
//         Filter
//-----------------------------------------------------------


#include "veins/mps/basic_checks/predictors/KalmanFilter.h"

KalmanFilter::
KalmanFilter()
{}



KalmanFilter::
KalmanFilter (
    const double       timeStep,
    const Parameters&  params,
    const int          stateVectorSize,
    const int          controlVectorSize,
    const int          measurementVectorSize)
: params_(params)
{
  setTimeStep             (timeStep);
  setStateVectorSize      (stateVectorSize);
  setControlVectorSize    (controlVectorSize);
  setMeasurementVectorSize(measurementVectorSize);

  setA();
  setB();
  setH();
  setR();
  setQ();
  setP();
  setW();

  // Initial states
  auto x0 = Eigen::VectorXd(getStateVectorSize());
  x0 << params_.getPosition().x, params_.getPosition().y, params_.getSpeed().x, params_.getSpeed().y;
  setX(x0);

  setI();
  setInitialized(true);
}



KalmanFilter::
~KalmanFilter()
{}



void KalmanFilter::
setA() {
    // State transition Matrix
  auto dt = getTimeStep();
  A_      = Eigen::MatrixXd(getStateVectorSize(), getStateVectorSize()); // System dynamics matrix

  A_ << 1, 0, dt, 0,
        0, 1, 0,      dt,
        0, 0, 1,      0,
        0, 0, 0,      1;
}



void KalmanFilter::
setB() {
    auto dt = getTimeStep();
    // Input Control Matrix (B_ = Eigen::MatrixXd::Identity(rows=n_states, cols=n_ctrl_states))
    B_ = Eigen::MatrixXd(getStateVectorSize(), getControlVectorSize()); // Control dynamics matrix

    B_ << 0.5 * dt * dt, 0,
          0            , 0.5 * dt * dt,
          dt           , 0,
          0            , dt;
//    B_.setZero();
}



void KalmanFilter::
setH() {
    // Measurement Output Matrix
//    H_ = Eigen::MatrixXd(measurementVectorSize, stateVectorSize); // Output matrix
//    H << 1, 0, 0, 0,
//         0, 1, 0, 0,
//         0, 0, 1, 0,
//         0, 0, 0, 1;
    H_ = Eigen::MatrixXd::Identity(getMeasurementVectorSize(), getStateVectorSize());
}



void KalmanFilter::
setQ() {
    // Action Uncertainty
    Q_ = Eigen::MatrixXd(getStateVectorSize(), getStateVectorSize()); // Process noise covariance
    Q_ = Eigen::MatrixXd::Identity(getStateVectorSize(), getStateVectorSize()) * 0.00001;
//    Q_ << 0.00001, 0,       0,       0,
//         0,       0.00001, 0,       0,
//         0,       0,       0.00001, 0,
//         0,       0,       0,       0.00001;
}



void KalmanFilter::
setR() {
    // Measurement Noise (R_ = Eigen::MatrixXd::Identity(n_states, n_ctrl_states) * 0.1)
//    R_ = Eigen::MatrixXd(measurementVectorSize, measurementVectorSize); // Measurement noise covariance
    R_ = Eigen::MatrixXd::Identity(getMeasurementVectorSize(), getMeasurementVectorSize()) * 0.1;
//    R << 0.1, 0  , 0  , 0,
//         0  , 0.1, 0  , 0,
//         0  , 0  , 0.1, 0,
//         0  , 0  , 0  , 0.1;
}



void KalmanFilter::
setP() {
    // Prediction Error (P_ = Eigen::MatrixXd::Identity(n_states, n_states) * 0.25)
//    P_ = Eigen::MatrixXd(stateVectorSize, stateVectorSize); // Estimate error covariance
    P_ = Eigen::MatrixXd::Identity(getStateVectorSize(), getStateVectorSize()) * 1000.0;
//    P << 0.25, 0   , 0   , 0,
//         0   , 0.25, 0   , 0,
//         0   , 0   , 0.25, 0,
//         0   , 0   , 0   , 0.25;
}



void KalmanFilter::
setW() {
//  Prediction Noise (W_ = Eigen::VectorXd::Constant(n_states, 0) -OR- W_.setZero())
//    W_ = Eigen::VectorXd::Constant(stateVectorSize, 0);
    W_ = Eigen::VectorXd(getStateVectorSize()); // Predicted state noise matrix
//    W << 0,
//         0,
//         0,
//         0;
}
