//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
// @author Raashid Ansari
// @date 04/06/2018

#ifndef MODULES_UNSCENTEDKALMANFILTER_H_
#define MODULES_UNSCENTEDKALMANFILTER_H_

#include "veins/mps/basic_checks/predictors/BaseUnscentedKalmanFilter.h"
#include "veins/mps/utility/Parameters.h"

class UnscentedKalmanFilter : public BaseUnscentedKalmanFilter {
public:
  explicit UnscentedKalmanFilter (
    const double       timeStep,
    const Parameters&  params,
    const int          stateVectorSize,
    const int          controlVectorSize,
    const int          measurementVectorSize);

  ~UnscentedKalmanFilter();

  void setA();
  void setB();
  void setH();
  void setR();
  void setP();
  void setQ();
  void setW();

private:
  Parameters params_;
};

#endif /* MODULES_UNSCENTEDKALMANFILTER_H_ */
