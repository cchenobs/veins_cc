

#ifndef MODULES_PREDICTORS_UNSCENTEDKALMANFITLER_H_
#define MODULES_PREDICTORS_UNSCENTEDKALMANFITLER_H_

#include <Eigen/Dense>

#include "veins/mps/basic_checks/predictors/BaseKalmanFilter.h"

class BaseUnscentedKalmanFilter : public BaseKalmanFilter
{
public:
    /**
   * @brief Create a Kalman filter with the specified matrices.
   *   A - System dynamics matrix
   *   B - Control dynamics matrix
   *   H - Output matrix
   *   Q - Process noise covariance
   *   R - Measurement noise covariance
   *   P - Estimate error covariance
   */
  BaseUnscentedKalmanFilter();
  virtual ~BaseUnscentedKalmanFilter();

  void setSigPred();
  void setWeights();
  void update(const Eigen::VectorXd& z, const Eigen::VectorXd& v);
  void predict(const Eigen::VectorXd& u);

protected:
	// predicted sigma points matrix
	Eigen::MatrixXd xSigPred_;

	// Weights of sigma points
	Eigen::VectorXd weights_;


};

#endif /* MODULES_PREDICTORS_UNSCENTEDKALMANFILTER_H_ */
