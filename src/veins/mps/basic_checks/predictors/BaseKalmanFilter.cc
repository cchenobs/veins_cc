/**
* Implementation of KalmanFilter class.
*
* @author: Hayk Martirosyan, Raashid Ansari
* @date: 04/06/2018 (mm/dd/yyyy)
*/

#include <exception>
#include <veins/mps/basic_checks/predictors/BaseKalmanFilter.h>

BaseKalmanFilter::
BaseKalmanFilter()
: initialized_(false)
{}



BaseKalmanFilter::
~BaseKalmanFilter()
{}



void BaseKalmanFilter::
setInitialized(const bool initialized) {
  initialized_ = initialized;
}



const bool BaseKalmanFilter::
isInitialized() {
  return initialized_;
}



void BaseKalmanFilter::
setX(Eigen::VectorXd& x0) {
  xHat_ = Eigen::VectorXd(stateVectorSize_);
  xHat_ = x0;
}



void BaseKalmanFilter::
setI() {
  I_ = Eigen::MatrixXd::Identity(stateVectorSize_, stateVectorSize_);
}



void BaseKalmanFilter::
init() {
  xHat_.setZero();
  initialized_ = true;
}



void BaseKalmanFilter::
step(const Eigen::VectorXd& u, const Eigen::VectorXd& z, const Eigen::VectorXd& v) {
  predict(u);
  update(z, v);
}



void BaseKalmanFilter::
update(const Eigen::VectorXd& z, const Eigen::VectorXd& v) {
  // update/Correction steps
  K_     = (P_ * H_.transpose()) * (H_ * P_ * H_.transpose() + R_).inverse();
  auto y = z + v - H_ * xHat_; // measurement to state relation function
  xHat_ += K_ * y;
  P_     = (I_ - K_ * H_) * P_;
}



/*
 * @brief Predict next state of the system
 * @author Raashid Ansari
 */
void BaseKalmanFilter::
predict(const Eigen::VectorXd& u) {
  if(!initialized_)
    throw std::runtime_error("Filter is not initialized!");

  // Prediction steps
  xHat_ = A_ * xHat_ + B_ * u + W_;
  P_    = A_ * P_ * A_.transpose() + Q_;
}



void BaseKalmanFilter::
setStateVectorSize(const int stateVectorSize) {
  stateVectorSize_ = stateVectorSize;
}



void BaseKalmanFilter::
setControlVectorSize(const int controlVectorSize) {
  controlVectorSize_ = controlVectorSize;
}



void BaseKalmanFilter::
setMeasurementVectorSize(const int measurementVectorSize) {
  measurementVectorSize_ = measurementVectorSize;
}



Eigen::VectorXd BaseKalmanFilter::
getState() {
  return xHat_;
};



int BaseKalmanFilter::
getStateVectorSize() {
  return stateVectorSize_;
}



int BaseKalmanFilter::
getControlVectorSize() {
  return controlVectorSize_;
}



int BaseKalmanFilter::
getMeasurementVectorSize() {
  return measurementVectorSize_;
}



void BaseKalmanFilter::
setTimeStep(const double timeStep) {
  timeStep_ = timeStep;
}



const double BaseKalmanFilter::
getTimeStep() {
  return timeStep_;
}



void BaseKalmanFilter::setA() {}
void BaseKalmanFilter::setB() {}
void BaseKalmanFilter::setH() {}
void BaseKalmanFilter::setQ() {}
void BaseKalmanFilter::setR() {}
void BaseKalmanFilter::setP() {}
void BaseKalmanFilter::setW() {}
