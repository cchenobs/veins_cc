//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MPS_BASIC_CHECKS_MAXDENSITYTHRESHOLD_H_
#define SRC_VEINS_MPS_BASIC_CHECKS_MAXDENSITYTHRESHOLD_H_

#include <veins/mps/basic_checks/ICheck.h>
#include <veins/mps/utility/SenderTable.h>
#include <veins/mps/utility/TypeDefs.h>

class MaxDensityThreshold : public ICheck
{
public:
  MaxDensityThreshold();
  virtual ~MaxDensityThreshold();
  void execute(const Coord& vehPos, Sender& sender, SenderTable& senderTable, double rectLen, double rectWidth, double vehLen, double vehWidth, const Coord& space);
protected:
  double triangleArea(const Triangle& t) const;
  bool isPointInRectangle(const Rectangle& r, const Coord& p) const;
};

#endif /* SRC_VEINS_MPS_BASIC_CHECKS_MAXDENSITYTHRESHOLD_H_ */
