//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/BrakeAcceleration.h>

BrakeAcceleration::BrakeAcceleration()
{
  // TODO Auto-generated constructor stub

}

BrakeAcceleration::~BrakeAcceleration()
{
  // TODO Auto-generated destructor stub
}



void BrakeAcceleration::
execute(Sender& sender) {
  auto brakeAppliedStatus = static_cast<u_int8_t>(sender.getBsm().getBrakeStatus());
  auto acceleration = sender.getBsm().getSenderAcceleration().length();
  const int ACCELERATION_UNAVAILABLE = 2001;

  // check if brake status is unavailable and acceleration is unavailable
  if (((brakeAppliedStatus & MASK) != MASK || acceleration == ACCELERATION_UNAVAILABLE)) {
    return;
  }

  // check if brake is applied
  bool isBrakeApplied = false;
  if ((brakeAppliedStatus & LEFT_FRONT ) == LEFT_FRONT  ||
      (brakeAppliedStatus & RIGHT_FRONT) == RIGHT_FRONT ||
      (brakeAppliedStatus & LEFT_REAR  ) == LEFT_REAR   ||
      (brakeAppliedStatus & RIGHT_REAR ) == RIGHT_REAR  ) {
    isBrakeApplied = true;
  }

  // check if vehicle is accelerating
  bool isAccelerating = false;
  if (acceleration > 0 && acceleration <= 2000) {
    isAccelerating = true;
  }

  // if vehicle is braking and accelerating, consider it as misbehavior
  sender.getInstantRating().setBa(isBrakeApplied && isAccelerating);
}
