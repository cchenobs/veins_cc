//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/ICheck.h>

ICheck::ICheck()
{}

ICheck::~ICheck()
{
}

double ICheck::getFadeFactor() const
{
  return fadeFactor_;
}

int ICheck::getGain() const
{
  return gain_;
}

void ICheck::setFadeFactor(double fadeFactor)
{
  fadeFactor_ = fadeFactor;
}

void ICheck::setGain(int gain)
{
  gain_ = gain;
}

const Latency& ICheck::getLatency() const
{
  return latency_;
}

void ICheck::setLatency(const Latency& latency)
{
  latency_ = latency;
}
