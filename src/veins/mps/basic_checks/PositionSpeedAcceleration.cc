//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/PositionSpeedAcceleration.h>

PositionSpeedAcceleration::PositionSpeedAcceleration()
{
  // TODO Auto-generated constructor stub

}

PositionSpeedAcceleration::~PositionSpeedAcceleration()
{
  // TODO Auto-generated destructor stub
}


void PositionSpeedAcceleration::
execute(Sender& sender, double threshold) {
  auto s0 = sender.getBsm().getSenderPos();
  if (!sender.isFirstBeacon()) {
    // Part 1:
    // get previous estimate of next position
    auto nextPos = sender.getNextPosEstimate();
    // compare distance between current and estimated positions against a drift threshold
    // output pass/fail value
    sender.getInstantRating().setPsa(nextPos.distance(s0) > threshold);
  }

  // Part 2:
  // get current position, speed
  auto u = sender.getBsm().getSenderSpeed();
  auto a = sender.getBsm().getSenderAcceleration();
  auto dt = simTime().dbl() - sender.getLastBeaconTime();
  // calculate next position estimate
  // s1 = s0 + u*t + (1/2)*a*t^2
  auto s1 = s0 + u*dt + a*dt*dt*0.5;
  // save into Sender
  sender.setNextPosEstimate(s1);
}
