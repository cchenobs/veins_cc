//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GHOSTCARS_AttackerApplLayer_H_
#define __GHOSTCARS_AttackerApplLayer_H_

#include <omnetpp/ccomponent.h>
#include <algorithm>
#include <map>

#include "veins/mps/traffic/TrafficManager.h"
#include "veins/mps/utility/Visuals.h"

#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/base/connectionManager/ConnectionManager.h"

#include "veins/mps/utility/rapidjson/writer.h"
#include "veins/mps/utility/rapidjson/stringbuffer.h"
#include "veins/mps/utility/FileLogger.h"

/**
 * Application layer for Attacker Car
 */
class AttackerApp : public BaseWaveApplLayer
{
public:
  AttackerApp ();
  ~AttackerApp();
  void initialize   (int stage) override;
  void finish       ()          override;
//  virtual int numInitStages() const { return std::max(cSimpleModule::numInitStages(), 4); }

protected:
  std::map<int, double> ghostVehDistMap_;
  cOutVector            ghostVehDistVec_;
  cOutVector            ghostVelocityVec_;
  cOutVector            attackDurationVec_;
  bool                  constantPositionAttackOffsetFlag_;
  Coord                 ghostPos_;

  TrafficManager*    trafficManager_;
  ConnectionManager* connManager_;
  cCanvas*           canvas_;
  uint32_t           beaconPriority;

  enum Attacks {
    ATTACK_NO,
    ATTACK_SUDDEN_APPEARANCE,
    ATTACK_CONSTANT_POSITION,
    ATTACK_BRAKE_COMM_RANGE,
    ATTACK_RANDOM_POSITION,
    ATTACK_DOS,
    ATTACK_FAKE_EEBL
  };

protected:
  void handleSelfMsg     (cMessage* msg)                          override;
  void handlePositionUpdate  (cObject* obj)            override;
  void onBSM             (BasicSafetyMessage* bsm)                override;
  void brakeFromCommRange(const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i);
  void suddenAppearance  (const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i);
  void constantPosition  (const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i);
  void randomPosition    (const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i);
  void dos               (const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i);
  void fakeEebl          (const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i);

private:
  Visuals visuals_;
  void populateAttackBsm(const BasicSafetyMessage* bsm, BasicSafetyMessage* attackBsm, const int ghostIndex);
  int attackFrom_;
  int attackUntil_;
  int attackerType_;
  int dosMessages_;
  Coord curAcceleration_;
  Coord curDirection_;
  Coord curOrientation_;
};

#endif
