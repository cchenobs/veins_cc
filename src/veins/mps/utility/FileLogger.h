/*
 * FileLogger.h
 *
 *  Created on: Oct 31, 2017
 *      Author: veins
 */

#ifndef UTILS_FILELOGGER_H_
#define UTILS_FILELOGGER_H_

#include <string>
#include <fstream>
#include <sstream>

class FileLogger
{
public:
  FileLogger();
  explicit FileLogger(
      const std::string& path,
      const std::string& fileName);
  explicit FileLogger(const std::string& fileName);
  FileLogger(const FileLogger& other);
  virtual ~FileLogger();
  FileLogger operator=(const FileLogger& other);

  const void log(const std::string& jsonObject);

private:
  std::ofstream fileHandler_;
};

#endif /* UTILS_FILELOGGER_H_ */
