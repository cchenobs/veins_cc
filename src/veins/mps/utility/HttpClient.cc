//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <sstream>
#include <istream>
#include <ostream>
#include <iostream>

#include <veins/mps/utility/HttpClient.h>

#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/StreamCopier.h>
#include <Poco/Path.h>
#include <Poco/URI.h>
#include <Poco/Exception.h>

HttpClient::HttpClient(std::string& uri)
: uri_(uri)
{}

HttpClient::~HttpClient()
{}

std::string HttpClient::
sendRequest(std::string reqBody, const std::string reqType) {
  try
  {
    // prepare session
    auto uri = Poco::URI(uri_);
    Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
    session.setKeepAlive(true);

    // prepare path
    auto path = uri.getPathAndQuery();
    if (path.empty())
      path = "/";

    if (reqType == Poco::Net::HTTPRequest::HTTP_POST) {
      Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
      req.setContentType("application/json");
      req.setKeepAlive(true);
      req.setContentLength( reqBody.length() );

      std::ostream& myOStream = session.sendRequest(req); // sends request, returns open stream
      myOStream << reqBody;  // sends the body
    } else if (reqType == Poco::Net::HTTPRequest::HTTP_PUT) {
      // send PUT request
      Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_PUT, path, Poco::Net::HTTPMessage::HTTP_1_1);
      req.setContentType("application/json");
      req.setKeepAlive(true);
      req.setContentLength( reqBody.length() );

      std::ostream& myOStream = session.sendRequest(req); // sends request, returns open stream
      myOStream << reqBody;  // sends the body
    }

//    req.write(std::cout);

    // get response
    Poco::Net::HTTPResponse res;
//    std::cout << res.getStatus() << " " << res.getReason() << std::endl;

    // print response
    std::istream& is = session.receiveResponse(res);
    auto os = std::ostringstream{};
    Poco::StreamCopier::copyStream(is, os);
    return os.str();
  }
  catch (Poco::Exception &ex)
  {
    std::cerr << ex.displayText() << std::endl;
    return "error";
  }
  return "done";
}
