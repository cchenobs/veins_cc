/*
 * Vehicle.h
 *
 *  Created on: Jun 29, 2018
 *      Author: veins
 */

#ifndef MODULES_VEHICLE_VEHICLE_H_
#define MODULES_VEHICLE_VEHICLE_H_

#include <string>

class Vehicle
{
public:
    Vehicle();
    ~Vehicle();
    Vehicle(std::string id, std::string type, int8_t lane, double position, double speed)
    :id_(id),
     type_(type),
     lane_(lane),
     position_(position),
     speed_(speed)
    {}

    const std::string& getId      () const;
    void               setId      (const std::string& id);

    const int8_t       getLane    () const;
    void               setLane    (int8_t lane);

    const double       getPosition() const;
    void               setPosition(double position);

    const double       getSpeed   () const;
    void               setSpeed   (double speed);

    const std::string& getType    () const;
    void               setType    (const std::string& type);

protected:
    std::string id_;
    std::string type_;
    int8_t      lane_;
    double      position_;
    double      speed_;
}; // class Vehicle

#endif /* MODULES_VEHICLE_VEHICLE_H_ */
