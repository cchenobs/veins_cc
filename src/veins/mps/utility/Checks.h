//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MPS_UTILITY_CHECKS_H_
#define SRC_VEINS_MPS_UTILITY_CHECKS_H_

#include <veins/mps/basic_checks/AcceptanceRangeThreshold.h>
#include <veins/mps/basic_checks/SuddenAppearanceWarning.h>
#include <veins/mps/basic_checks/MaxBeaconingFrequency.h>
#include <veins/mps/basic_checks/PlausibleVelocity.h>
#include <veins/mps/basic_checks/MaxVelocity.h>
#include <veins/mps/basic_checks/PlausibleAcceleration.h>
#include <veins/mps/basic_checks/MaxAcceleration.h>
#include <veins/mps/basic_checks/Kalman.h>
#include <veins/mps/basic_checks/MinimumDistanceMoved.h>
#include <veins/mps/basic_checks/FirstBSMRSSI.h>
#include <veins/mps/basic_checks/PositionSpeedAcceleration.h>
#include <veins/mps/basic_checks/SpeedAcceleration.h>
#include <veins/mps/basic_checks/PositionOverlap.h>
#include <veins/mps/basic_checks/MaxDensityThreshold.h>
#include <veins/mps/basic_checks/BrakeAcceleration.h>
#include <veins/mps/basic_checks/LocationPlausiblity.h>
#include <veins/mps/basic_checks/MovementPlausibility.h>

class Checks
{
public:
  Checks();
  Checks(
    AcceptanceRangeThreshold art,
    SuddenAppearanceWarning saw,
    MaxBeaconingFrequency mbf,
    PlausibleVelocity mvp,
    MaxVelocity mvn,
    PlausibleAcceleration map,
    MaxAcceleration man,
    FirstBSMRSSI fbr,
    Kalman kalman,
    PositionSpeedAcceleration psa,
    SpeedAcceleration sa,
    PositionOverlap po,
    MaxDensityThreshold mdt,
    BrakeAcceleration ba,
    LocationPlausiblity lp,
    MovementPlausibility mp1,
    MovementPlausibility mp2
    );

  Checks& operator=(const Checks& other);

  virtual ~Checks();
  AcceptanceRangeThreshold& getArt();
  Kalman& getKalman();
  MaxAcceleration& getMan();
  PlausibleAcceleration& getMap();
  MaxVelocity& getMvn();
  PlausibleVelocity& getMvp();
  MaxBeaconingFrequency& getMbf();
  SuddenAppearanceWarning& getSaw();
  FirstBSMRSSI& getFbr();
  PositionSpeedAcceleration& getPsa();
  SpeedAcceleration& getSa();
  PositionOverlap& getPo();
  MaxDensityThreshold& getMdt();
  BrakeAcceleration& getBa();
  LocationPlausiblity& getLp();
  MovementPlausibility& getMp1();
  MovementPlausibility& getMp2();

  void setArt(const AcceptanceRangeThreshold& art);
  void setKalman(const Kalman& kalman);
  void setMan(const MaxAcceleration& man);
  void setMap(const PlausibleAcceleration& map);
  void setMvn(const MaxVelocity& mvn);
  void setMvp(const PlausibleVelocity& mvp);
  void setMbf(const MaxBeaconingFrequency& mbf);
  void setSaw(const SuddenAppearanceWarning& saw);
  void setFbr(const FirstBSMRSSI& fbr);
  void setPsa(const PositionSpeedAcceleration& psa);
  void setSa(const SpeedAcceleration& sa);
  void setPo(const PositionOverlap& po);
  void setMdt(const MaxDensityThreshold& mdt);
  void setBa(const BrakeAcceleration& ba);
  void setLp(const LocationPlausiblity& lp);
  void setMp1(const MovementPlausibility& mp1);
  void setMp2(const MovementPlausibility& mp2);

private:
  AcceptanceRangeThreshold  art_;
  SuddenAppearanceWarning   saw_;
  MaxBeaconingFrequency     mbf_;
  PlausibleVelocity         mvp_;
  MaxVelocity               mvn_;
  PlausibleAcceleration     map_;
  MaxAcceleration           man_;
  Kalman                    kalman_;
  FirstBSMRSSI              fbr_;
  PositionSpeedAcceleration psa_;
  SpeedAcceleration         sa_;
  PositionOverlap           po_;
  MaxDensityThreshold       mdt_;
  BrakeAcceleration         ba_;
  LocationPlausiblity       lp_;
  MovementPlausibility      mp1_, mp2_;
};

#endif /* SRC_VEINS_MPS_UTILITY_CHECKS_H_ */
