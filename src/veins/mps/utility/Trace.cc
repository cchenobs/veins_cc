//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/utility/Trace.h>
#include <veins/mps/utility/CSVWriter/include/CSVWriter.h>
#include "veins/mps/utility/rapidjson/writer.h"
#include "veins/mps/utility/rapidjson/stringbuffer.h"
#include <veins/mps/utility/FileLogger.h>
//#include <veins/mps/utility/HttpClient.h>
//#include <Poco/Net/HTTPRequest.h>

std::string dummyServerAddress = "0";

Trace::Trace(const std::string& absolutePathofFile, std::string& dataServerAddress)
: fName_(absolutePathofFile),
  dataServerAddress_(dataServerAddress)
{}

Trace::Trace(const std::string& absolutePathofFile)
: fName_(absolutePathofFile),
  dataServerAddress_(dummyServerAddress)
{}

Trace::~Trace()
{}

void Trace::printCsv() {
  auto csv = CSVWriter(",");

  csv <<
      rvId_ <<
      hvId_ <<
      simTime_ <<
      kalman_.x_ <<
      kalman_.y_ <<
      pos_.x_ <<
      pos_.y_ <<
      gps_.x_ <<
      gps_.y_ <<
      posSD_.x_ <<
      posSD_.y_ <<
      vel_.x_ <<
      vel_.y_ <<
      distance_ <<
      distanceSD_ <<
      ewma_.getArt() <<
      ewma_.getMbf() <<
      ewma_.getSaw() <<
      ewma_.getMvp() <<
      ewma_.getMvn() <<
      ewma_.getMap() <<
      ewma_.getMan() <<
      ewma_.getMdm() <<
      ewma_.getKalman() <<
      ewma_.getPsa() <<
      ewma_.getSa() <<
      ewma_.getPo() <<
      ewma_.getMdt() <<
      ewma_.getBa() <<
      ewma_.getLp() <<
      ewma_.getMp1() <<
      ewma_.getMp2() <<
      ewma_.getFbr() <<
      ewma_.getSuspicion() <<
      instant_.getArt() <<
      instant_.getMbf() <<
      instant_.getSaw() <<
      instant_.getMvp() <<
      instant_.getMvn() <<
      instant_.getMap() <<
      instant_.getMan() <<
      instant_.getMdm() <<
      instant_.getKalman() <<
      instant_.getPsa() <<
      instant_.getSa() <<
      instant_.getPo() <<
      instant_.getMdt() <<
      instant_.getBa() <<
      instant_.getLp() <<
      instant_.getMp1() <<
      instant_.getMp2() <<
      instant_.getFbr() <<
      instant_.getSuspicion() <<
      suspectedAttack_ <<
      eeblMpsOff_ <<
      eeblMpsOn_ <<
      rssi_ <<
      mlEwmaPrediction_ <<
      mlInstantPrediction_ <<
      wsmData_.c_str() <<
      latency_.getElapsedSimTime() <<
      latency_.getElapsedTime() <<
      latency_.getBsm() <<
      latency_.getDistance() <<
      checks_.getArt().execTime_ <<
      checks_.getSaw().execTime_ <<
      checks_.getMbf().execTime_ <<
      checks_.getMvp().execTime_ <<
      checks_.getMvn().execTime_ <<
      checks_.getMap().execTime_ <<
      checks_.getMan().execTime_ <<
      checks_.getFbr().execTime_ <<
      checks_.getPsa().execTime_ <<
      checks_.getSa().execTime_ <<
      checks_.getPo().execTime_ <<
      checks_.getMdt().execTime_ <<
      checks_.getBa().execTime_ <<
      checks_.getLp().execTime_ <<
      checks_.getMp1().execTime_ <<
      checks_.getMp2().execTime_ <<
      checks_.getKalman().execTime_ <<
      mlInstantExecTime_ <<
      mlEwmaExecTime_ <<
      attackGt_ <<
      attackType_.c_str() <<
      suspicionThreshold_ <<
      repNum_;

  auto fileName = std::ostringstream{};
  fileName << fName_ << ".csv";

  csv.writeToFile(fileName.str().c_str(), true);

  // build request in JSON format
  auto s = rapidjson::StringBuffer{};
  auto writer = rapidjson::Writer<rapidjson::StringBuffer>(s);
  writer.StartObject();
    writer.Key("bsm");
    writer.String(csv.toString().c_str());
  writer.EndObject();

  // connect to server and send request
//  auto hc = HttpClient(dataServerAddress_);
//  hc.sendRequest(s.GetString(), Poco::Net::HTTPRequest::HTTP_POST);
}

void Trace::printJson() {
  // *************************************
  // LOGGING
  // *************************************
  // Record position, distance trace and standard deviation in external file
  auto s = rapidjson::StringBuffer{};
  auto writer = rapidjson::Writer<rapidjson::StringBuffer>(s);

  writer.StartObject();

    writer.Key("rvId");
    writer.Int(rvId_);

    writer.Key("hvId");
    writer.Int(hvId_);

    writer.Key("simTime");
    writer.Double(simTime_);

    writer.Key("kalman");
    writer.StartObject();
      writer.Key("x");
      writer.Double(kalman_.x_);
      writer.Key("y");
      writer.Double(kalman_.y_);
    writer.EndObject();

    writer.Key("pos");
    writer.StartObject();
      writer.Key("x");
      writer.Double(pos_.x_);
      writer.Key("y");
      writer.Double(pos_.y_);
    writer.EndObject();

  // get position as SUMO/GPS coordinates instead of Omnet++ coordinates
    writer.Key("gps");
    writer.StartObject();
      writer.Key("x");
      writer.Double(gps_.x_);
      writer.Key("y");
      writer.Double(gps_.y_);
    writer.EndObject();


  // update sender's position standard deviation value
    writer.Key("posSD");
    writer.StartObject();
      writer.Key("x");
      writer.Double(posSD_.x_);
      writer.Key("y");
      writer.Double(posSD_.y_);
    writer.EndObject();

    writer.Key("vel");
    writer.StartObject();
      writer.Key("x");
      writer.Double(vel_.x_);
      writer.Key("y");
      writer.Double(vel_.y_);
    writer.EndObject();

    writer.Key("distance");
    writer.Double(distance_);

  // update sender's distance standard deviation value
    writer.Key("distanceSD");
    writer.Double(distanceSD_);

    writer.Key("ratings");
    writer.StartObject();
      writer.Key("ewma");
      writer.StartObject();
        writer.Key("ART");
        writer.Double(ewma_.getArt());

        writer.Key("MBF");
        writer.Double(ewma_.getMbf());

        writer.Key("SAW");
        writer.Double(ewma_.getSaw());

        writer.Key("MVP");
        writer.Double(ewma_.getMvp());

        writer.Key("MVN");
        writer.Double(ewma_.getMvn());

        writer.Key("MAP");
        writer.Double(ewma_.getMap());

        writer.Key("MAN");
        writer.Double(ewma_.getMan());

        writer.Key("MDM");
        writer.Double(ewma_.getMdm());

        writer.Key("KALMAN");
        writer.Double(ewma_.getKalman());

        writer.Key("PSA");
        writer.Double(ewma_.getPsa());

        writer.Key("SA");
        writer.Double(ewma_.getSa());

        writer.Key("PO");
        writer.Double(ewma_.getPo());

        writer.Key("MDT");
        writer.Double(ewma_.getMdt());

        writer.Key("BA");
        writer.Double(ewma_.getBa());

        writer.Key("LP");
        writer.Double(ewma_.getLp());

        writer.Key("MP1");
        writer.Double(ewma_.getMp1());

        writer.Key("MP2");
        writer.Double(ewma_.getMp2());

        writer.Key("FBR");
        writer.Double(ewma_.getFbr());

        writer.Key("suspicion");
        writer.Double(ewma_.getSuspicion());
      writer.EndObject();

      writer.Key("instant");
      writer.StartObject();
        writer.Key("ART");
        writer.Double(instant_.getArt());

        writer.Key("MBF");
        writer.Double(instant_.getMbf());

        writer.Key("SAW");
        writer.Double(instant_.getSaw());

        writer.Key("MVP");
        writer.Double(instant_.getMvp());

        writer.Key("MVN");
        writer.Double(instant_.getMvn());

        writer.Key("MAP");
        writer.Double(instant_.getMap());

        writer.Key("MAN");
        writer.Double(instant_.getMan());

        writer.Key("MDM");
        writer.Double(instant_.getMdm());

        writer.Key("KALMAN");
        writer.Double(instant_.getKalman());

        writer.Key("PSA");
        writer.Double(instant_.getPsa());

        writer.Key("SA");
        writer.Double(instant_.getSa());

        writer.Key("PO");
        writer.Double(instant_.getPo());

        writer.Key("MDT");
        writer.Double(instant_.getMdt());

        writer.Key("BA");
        writer.Double(instant_.getBa());

        writer.Key("LP");
        writer.Double(instant_.getLp());

        writer.Key("MP1");
        writer.Double(instant_.getMp1());

        writer.Key("MP2");
        writer.Double(instant_.getMp2());

        writer.Key("FBR");
        writer.Double(instant_.getFbr());

        writer.Key("suspicion");
        writer.Double(instant_.getSuspicion());
      writer.EndObject();
    writer.EndObject();

    writer.Key("suspectedAttack");
    writer.Bool(suspectedAttack_);

    writer.Key("eebl");
    writer.StartObject();
      writer.Key("mpsOff");
      writer.Int(eeblMpsOff_);

      writer.Key("mpsOn");
      writer.Int(eeblMpsOn_);
    writer.EndObject();

    writer.Key("rssi");
    writer.Double(rssi_);

    writer.Key("mlEwmaPrediction");
    writer.Int(mlEwmaPrediction_);

    writer.Key("mlInstantPrediction");
    writer.Int(mlInstantPrediction_);

    writer.Key("wsmData");
    writer.String(wsmData_.c_str());

  // log latency values
    writer.Key("latency");
    writer.StartObject();
      writer.Key("simTime");
      writer.Double(latency_.getElapsedSimTime());

      writer.Key("realTime");
      writer.Double(latency_.getElapsedTime());

      writer.Key("bsm");
      writer.Double(latency_.getBsm());

      writer.Key("distance");
      writer.Double(latency_.getDistance());
    writer.EndObject();

    // log execution time values
    writer.Key("execTime");
    writer.StartObject();
      writer.Key("ART");
      writer.Double(checks_.getArt().execTime_);

      writer.Key("SAW");
      writer.Double(checks_.getSaw().execTime_);

      writer.Key("MBF");
      writer.Double(checks_.getMbf().execTime_);

      writer.Key("MVP");
      writer.Double(checks_.getMvp().execTime_);

      writer.Key("MVN");
      writer.Double(checks_.getMvn().execTime_);

      writer.Key("MAP");
      writer.Double(checks_.getMap().execTime_);

      writer.Key("MAN");
      writer.Double(checks_.getMan().execTime_);

      writer.Key("PSA");
      writer.Double(checks_.getPsa().execTime_);

      writer.Key("SA");
      writer.Double(checks_.getSa().execTime_);

      writer.Key("PO");
      writer.Double(checks_.getPo().execTime_);

      writer.Key("MDT");
      writer.Double(checks_.getMdt().execTime_);

      writer.Key("FBR");
      writer.Double(checks_.getFbr().execTime_);

      writer.Key("BA");
      writer.Double(checks_.getBa().execTime_);

      writer.Key("LP");
      writer.Double(checks_.getLp().execTime_);

      writer.Key("MP1");
      writer.Double(checks_.getMp1().execTime_);

      writer.Key("MP2");
      writer.Double(checks_.getMp2().execTime_);

      writer.Key("KALMAN");
      writer.Double(checks_.getKalman().execTime_);

      writer.Key("IML");
      writer.Double(mlInstantExecTime_);

      writer.Key("EML");
      writer.Double(mlEwmaExecTime_);
    writer.EndObject();

    writer.Key("groundTruth");
    writer.StartObject();
      writer.Key("attackGt");
      writer.Bool(attackGt_);

      writer.Key("attackType");
      writer.String(attackType_.c_str());

      writer.Key("threshold");
      writer.Int(suspicionThreshold_);

      writer.Key("repNum");
      writer.Int(repNum_);
    writer.EndObject();

  writer.EndObject();

  auto fileName = std::ostringstream{};
  fileName << fName_ << ".json";

  auto fl = FileLogger(fileName.str());
  fl.log(s.GetString());
}

void Trace::printVehicleDensityCsv() {
  auto csv = CSVWriter(",");
  csv <<
      simTime_ <<
      vehicleDensity_ <<
      repNum_;

  auto fileName = std::ostringstream{};
  fileName << fName_ << ".csv";
  csv.writeToFile(fileName.str().c_str(), true);

  // build request in JSON format
  auto s = rapidjson::StringBuffer{};
  auto writer = rapidjson::Writer<rapidjson::StringBuffer>(s);
  writer.StartObject();
    writer.Key("vehicleDensity");
    writer.String(csv.toString().c_str());
  writer.EndObject();

  // connect to server and send request
//  auto hc = HttpClient(dataServerAddress_);
//  hc.sendRequest(s.GetString(), Poco::Net::HTTPRequest::HTTP_POST);
}

void Trace::printVehicleDensityJson() {
  auto s = rapidjson::StringBuffer{};
  auto writer = rapidjson::Writer<rapidjson::StringBuffer>(s);

  writer.StartObject();
    writer.Key("simTime");
    writer.Double(simTime_);

    writer.Key("vehicleDensity");
    writer.Uint(vehicleDensity_);

    writer.Key("repNum");
    writer.Uint(repNum_);
  writer.EndObject();

  auto fileName = std::ostringstream{};
  fileName << fName_ << ".json";

  auto fl = FileLogger(fileName.str());
  fl.log(s.GetString());
}

void Trace::setAttackGt(bool attackGt)
{
  attackGt_ = attackGt;
}

void Trace::setAttackType(const std::string& attackType)
{
  attackType_ = attackType;
}

void Trace::setChecks(const Checks& checks)
{
  checks_ = checks;
}

void Trace::setDistance(double distance)
{
  distance_ = distance;
}

void Trace::setDistanceSd(double distanceSd)
{
  distanceSD_ = distanceSd;
}

void Trace::setEeblMpsOff(int eeblMpsOff)
{
  eeblMpsOff_ = eeblMpsOff;
}

void Trace::setEeblMpsOn(int eeblMpsOn)
{
  eeblMpsOn_ = eeblMpsOn;
}

void Trace::setEwma(const Rating& ewma)
{
  ewma_ = ewma;
}

void Trace::setGps(double gpsX, double gpsY)
{
  gps_.x_ = gpsX;
  gps_.y_ = gpsY;
}

void Trace::setHvId(int hvId)
{
  hvId_ = hvId;
}

void Trace::setInstant(const Rating& instant)
{
  instant_ = instant;
}

void Trace::setKalman(double kalmanX, double kalmanY)
{
  kalman_.x_ = kalmanX;
  kalman_.y_ = kalmanY;
}

void Trace::setLatency(const Latency& latency)
{
  latency_ = latency;
}

void Trace::setPos(double posX, double posY)
{
  pos_.x_ = posX;
  pos_.y_ = posY;
}

void Trace::setPosSd(double posSdX, double posSdY)
{
  posSD_.x_ = posSdX;
  posSD_.y_ = posSdY;
}

void Trace::setRssi(double rssi)
{
  rssi_ = rssi;
}

void Trace::setRvId(int rvId)
{
  rvId_ = rvId;
}

void Trace::setSimTime(double simTime)
{
  simTime_ = simTime;
}

void Trace::setSuspectedAttack(bool suspectedAttack)
{
  suspectedAttack_ = suspectedAttack;
}

void Trace::setSuspicionThreshold(int suspicionThreshold)
{
  suspicionThreshold_ = suspicionThreshold;
}

void Trace::setVel(double velX, double velY)
{
  vel_.x_ = velX;
  vel_.y_ = velY;
}

void Trace::setWsmData(const std::string& wsmData)
{
  wsmData_ = wsmData;
}

void Trace::setRepNum(int repNum)
{
  repNum_ = repNum;
}

void Trace::setVehicleDensity(int vehicleDensity)
{
  vehicleDensity_ = vehicleDensity;
}

void Trace::setMlEwmaExecTime(double mlEwmaExecTime)
{
  mlEwmaExecTime_ = mlEwmaExecTime;
}

void Trace::setMlInstantExecTime(double mlInstantExecTime)
{
  mlInstantExecTime_ = mlInstantExecTime;
}

void Trace::setMlEwmaPrediction(int mlEwmaPrediction)
{
  mlEwmaPrediction_ = mlEwmaPrediction;
}

void Trace::setMlInstantPrediction(int mlInstantPrediction)
{
  mlInstantPrediction_ = mlInstantPrediction;
}
