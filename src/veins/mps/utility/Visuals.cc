/*
 * Visuals.cc
 *
 *  Created on: Aug 24, 2017
 *      Author: veins
 */

#include "veins/mps/utility/Visuals.h"

#include <omnetpp/ccomponent.h>

#include "veins/mps/utility/GhostVehicle.h"

Visuals::
Visuals()
{}



Visuals::
Visuals(const cModule* car)
: car_(car)
{}



Visuals::
~Visuals()
{}



bool Visuals::
shouldVisualize() const {
  return getEnvir()->isGUI() &&
      !getEnvir()->isExpressMode() &&
      car_ != nullptr;
}

void Visuals::
showBSMBubble(const int senderId) const {
  if (!shouldVisualize())
    return;

  auto ss = std::ostringstream{};
  ss << "BSM from " << senderId;
  car_->bubble(ss.str().c_str());
}



void Visuals::
updateRing(const int radius, const std::string& color) const {
  if (!shouldVisualize())
    return;

  auto ss = std::ostringstream{};
  ss << "r=" << radius << "," << color;
  car_->getDisplayString().updateWith(ss.str().c_str());
}



void Visuals::
updateGhostVisuals(
        const BasicSafetyMessage& bsm,
        const BasicSafetyMessage& spoofedBsm,
        cCanvas& canvas
        ) {
  if (!shouldVisualize())
    return;

  auto ss = std::ostringstream{};
  ss << "ghost_" << car_->getId() << "_" << bsm.getSenderAddress();
  auto ghostCarName = ss.str().c_str();
  auto ghostCarFigure = canvas.getFigure(ghostCarName);

  auto newGhostPos = cFigure::Point(spoofedBsm.getSenderPos().x, spoofedBsm.getSenderPos().y);

  // If ghost already present: get ghost's figure, update its position
  if (ghostCarFigure != nullptr) {
    auto ghost = static_cast<cRectangleFigure*>(ghostCarFigure);
    ghost->setPosition(newGhostPos, cFigure::ANCHOR_CENTER);
  }
  // If ghost's figure not present: create ghost figure, update position, add to the canvas
  else if (bsm.getWsmData() != std::string("ghost")) {
    auto ghostVehObj = cars::GhostVehicle{};
    ghostVehObj.createVehicle(canvas, ss.str());
    ghostVehObj.updatePosition(newGhostPos);
  }
}



void Visuals::
plotPredictions(
        const int nPredictions,
        const int senderId,
        cCanvas& canvas,
        const std::vector<Eigen::VectorXd>& predictions
        ) {
  if (!shouldVisualize())
    return;

  for (auto i = 0; i < nPredictions; ++i) {
    auto ss = std::ostringstream{};
    ss << "estimate_" << senderId << "_" << i;
    auto estimateName = ss.str().c_str();
    auto estimateFigure = canvas.getFigure(estimateName);

    // if figure already present, get figure, update position
    if (estimateFigure != nullptr) {
      auto estimate = static_cast<cRectangleFigure*>(estimateFigure);
      auto point    = cFigure::Point(predictions.at(i)[0], predictions.at(i)[1]);
      estimate->setPosition(point, cFigure::ANCHOR_CENTER);
    }
    // if figure not present, create figure, fill figure with color, update position, and add to the canvas
    else {
      auto estimate = new cRectangleFigure(estimateName);
      estimate->setBounds(cFigure::Rectangle(0, 0, 2, 2));

      cFigure::Color color = (i == 0) ? cFigure::GREEN : cFigure::RED;
      estimate->setFillColor(color);
      estimate->setFilled(true);
      estimate->setFillOpacity(0.5);

      auto point = cFigure::Point(predictions.at(i)[0], predictions.at(i)[1]);
      estimate->setPosition(point, cFigure::ANCHOR_CENTER);

      canvas.addFigure(estimate);
//      delete(estimate);
    }
  }
}
