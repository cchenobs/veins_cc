/*
 * FileLogger.cc
 *
 *  Created on: Oct 31, 2017
 *      Author: veins
 */

#include <iostream>
#include <sys/stat.h> // check if directory exists (S_ISDIR())

#include "FileLogger.h"



FileLogger::
FileLogger() {}



FileLogger::
FileLogger(const FileLogger& other) {
}



FileLogger::
FileLogger(
    const std::string& path,
    const std::string& fileName)
{
  fileHandler_.open(path + fileName, std::ios::app);
  if (!fileHandler_.is_open())
    throw std::runtime_error("Could not open file: " + path + fileName);
}



FileLogger::
FileLogger(
    const std::string& fileName)
{
  fileHandler_.open(fileName, std::ios::app);
  if (!fileHandler_.is_open())
    throw std::runtime_error("Could not open file: " + fileName);
}



FileLogger::
~FileLogger() {
  fileHandler_.close();
}



const void FileLogger::
log(const std::string& logString) {
  fileHandler_ << logString << std::endl;
}



FileLogger FileLogger::operator=(const FileLogger& other) {
  return *this;
}
