/*
 * Rating.cc
 *
 *  Created on: Oct 16, 2018
 *      Author: mps
 */

#include <veins/mps/utility/Rating.h>

Rating::
Rating()
: kalman_(0.0),
  art_(0.0),
  saw_(0.0),
  mdm_(0.0),
  mbf_(0.0),
  mvp_(0.0),
  mvn_(0.0),
  map_(0.0),
  man_(0.0),
  fbr_(0.0),
  psa_(0.0),
  sa_(0.0),
  po_(0.0),
  mdt_(0.0),
  ba_(0.0),
  lp_(0.0),
  mp1_(0.0),
  mp2_(0.0),
  mbsm_(0.0)
{}

Rating::
~Rating()
{}

double Rating::getArt() const
{
  return art_;
}

void Rating::setArt(double art)
{
  art_ = art;
}

double Rating::getKalman() const
{
  return kalman_;
}

void Rating::setKalman(double kalman)
{
  kalman_ = kalman;
}

double Rating::getMan() const
{
  return man_;
}

void Rating::setMan(double man)
{
  man_ = man;
}

double Rating::getMap() const
{
  return map_;
}

void Rating::setMap(double map)
{
  map_ = map;
}

double Rating::getMvn() const
{
  return mvn_;
}

void Rating::setMvn(double mvn)
{
  mvn_ = mvn;
}

double Rating::getMvp() const
{
  return mvp_;
}

void Rating::setMvp(double mvp)
{
  mvp_ = mvp;
}

double Rating::getMbf() const
{
  return mbf_;
}

void Rating::setMbf(double mbf)
{
  mbf_ = mbf;
}

double Rating::getMdm() const
{
  return mdm_;
}

void Rating::setMdm(double mdm)
{
  mdm_ = mdm;
}

double Rating::getSaw() const
{
  return saw_;
}

void Rating::setSaw(double saw)
{
  saw_ = saw;
}

double Rating::getFbr() const
{
  return fbr_;
}

void Rating::setFbr(double fbr)
{
  fbr_ = fbr;
}

double Rating::getPsa() const
{
  return psa_;
}

void Rating::setPsa(double psa)
{
  psa_ = psa;
}

double Rating::getSa() const
{
  return sa_;
}

void Rating::setSa(double sa)
{
  sa_ = sa;
}

double Rating::getPo() const
{
  return po_;
}

void Rating::setPo(double po)
{
  po_ = po;
}

double Rating::getMdt() const
{
  return mdt_;
}

void Rating::setMdt(double mdt)
{
  mdt_ = mdt;
}

double Rating::getBa() const
{
  return ba_;
}

void Rating::setBa(double ba)
{
  ba_ = ba;
}

double Rating::getLp() const
{
  return lp_;
}

void Rating::setLp(double lp)
{
  this->lp_ = lp;
}

double Rating::getMp1() const
{
  return mp1_;
}

void Rating::setMp1(double mp1)
{
  this->mp1_ = mp1;
}

double Rating::getMp2() const
{
  return mp2_;
}

void Rating::setMp2(double mp2)
{
  this->mp2_ = mp2;
}

double Rating::getMbsm() const
{
  return mbsm_;
}

void Rating::setMbsm(double mbsm)
{
  mbsm_ = mbsm;
}

double Rating::getSuspicion() const
{
  return (
      kalman_ +
      art_ +
      saw_ +
      mdm_ +
      mbf_ +
      mvp_ +
      mvn_ +
      map_ +
      man_ +
      fbr_ +
      psa_ +
      sa_  +
      po_  +
      mdt_ +
      ba_  +
      lp_  +
      mp1_ +
      mp2_ +
      mbsm_) / 19;
}
