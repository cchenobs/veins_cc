/*
 * Vehicle.cc
 *
 *Created on: Jun 6, 2017
 *    Author: Raashid Ansari
 */

#include "veins/mps/utility/Car.h"

u_int cars::Car::carCount_       = 0;
u_int cars::Car::normalCarCount_ = 0;
u_int cars::Car::attackerCarCount_ = 0;

cars::Car::
Car() {}

cars::Car::
Car(std::string carType)
: carType_(carType),
  simulationId_(carCount_++)
{
  if (carType_ == "normal")
    normalCarCount_++;
  else if (carType_ == "attacker")
    attackerCarCount_++;
  else
    cRuntimeError("Invalid car type entered!");
}



cars::Car::
~Car() {
  carCount_--;
  if (carType_ == "normal")
    normalCarCount_--;
  else if (carType_ == "attacker")
    attackerCarCount_--;
  else
    cRuntimeError("Invalid car type entered!");
}



const std::string cars::Car::
getGivenId() const {
  auto car_name = std::ostringstream{};
  car_name << carType_ << this->simulationId_;
  return car_name.str();
}



void cars::Car::
setGivenId(std::string carType, int simulationId) {
  carType_      = carType;
  simulationId_ = simulationId;
}



const u_int cars::Car::
getCarCount(const char carType) const {
  if (carType == 'n')
    return normalCarCount_;
  else if (carType == 'a')
    return attackerCarCount_;
  else
    return carCount_;
}



const int cars::Car::
getSimulationId() const {
  return simulationId_;
}



void cars::Car::
setSimulationId(const int simulationId) {
  simulationId_ = simulationId;
}



const std::string& cars::Car::
getCarType() const {
  return carType_;
}



void cars::Car::
setCarType(const std::string& carType) {
  carType_ = carType;
}



cModule* cars::Car::
getSimulationModule() const {
  return simulationModule_;
}



void cars::Car::
setSimulationModule(cModule* simulationModule) {
  simulationModule_ = simulationModule;
}
