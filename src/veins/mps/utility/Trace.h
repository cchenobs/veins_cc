//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MPS_UTILITY_TRACE_H_
#define SRC_VEINS_MPS_UTILITY_TRACE_H_

#include <string>

#include <veins/mps/utility/Rating.h>
#include <veins/mps/utility/Latency.h>
#include <veins/mps/utility/Checks.h>

class Trace
{
public:
  explicit Trace(const std::string& absolutePathofFile, std::string& dataServerAddress);
  explicit Trace(const std::string& absolutePathofFile);
  virtual ~Trace();

  struct XY {
    double x_;
    double y_;
  };

  void setAttackGt(bool attackGt);
  void setSuspectedAttack(bool suspectedAttack);
  void setAttackType(const std::string& attackType);
  void setChecks(const Checks& checks);
  void setDistance(double distance);
  void setDistanceSd(double distanceSd);
  void setEeblMpsOff(int eeblMpsOff);
  void setEeblMpsOn(int eeblMpsOn);
  void setEwma(const Rating& ewma);
  void setGps(double gpsX, double gpsY);
  void setHvId(int hvId);
  void setInstant(const Rating& instant);
  void setKalman(double kalmanX, double kalmanY);
  void setLatency(const Latency& latency);
  void setMlEwmaPrediction(int mlEwmaPrediction);
  void setMlInstantPrediction(int mlInstantPrediction);
  void setPos(double posX, double posY);
  void setPosSd(double posSdX, double posSdY);
  void setRssi(double rssi);
  void setRvId(int rvId);
  void setSimTime(double simTime);
  void setSuspicionThreshold(int suspicionThreshold);
  void setVel(double velX, double velY);
  void setWsmData(const std::string& wsmData);
  void setRepNum(int repNum);
  void setVehicleDensity(int vehicleDensity);
  void setMlEwmaExecTime(double mlEwmaExecTime);
  void setMlInstantExecTime(double mlInstantExecTime);

  void printCsv();
  void printJson();
  void printVehicleDensityCsv();
  void printVehicleDensityJson();

private:
  // Variables to define logging path
  const std::string& fName_;
  std::string& dataServerAddress_;

  // Variable to hold data
  int rvId_;
  int hvId_;
  double simTime_;
  XY kalman_;
  XY pos_;
  XY gps_;
  XY posSD_;
  XY vel_;
  double distance_;
  double distanceSD_;
  Rating ewma_;
  Rating instant_;
  bool suspectedAttack_;
  int eeblMpsOff_;
  int eeblMpsOn_;
  double rssi_;
  int mlInstantPrediction_;
  int mlEwmaPrediction_;
  std::string wsmData_;
  Latency latency_;
  Checks checks_;
  double mlInstantExecTime_;
  double mlEwmaExecTime_;
  bool attackGt_;
  std::string attackType_;
  int suspicionThreshold_;
  int repNum_;
  int vehicleDensity_;
};

#endif /* SRC_VEINS_MPS_UTILITY_TRACE_H_ */
