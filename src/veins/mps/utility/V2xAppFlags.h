/*
 * V2xAppFlags.h
 *
 *  Created on: Oct 16, 2018
 *      Author: mps
 */

#ifndef SRC_VEINS_MPS_MODULES_UTILITY_V2XAPPFLAGS_H_
#define SRC_VEINS_MPS_MODULES_UTILITY_V2XAPPFLAGS_H_

class V2xAppFlags
{
public:
  V2xAppFlags();
  virtual ~V2xAppFlags();
  bool isDnpwAfter() const;
  void setDnpwAfter(bool dnpwAfter);
  bool isDnpwBefore() const;
  void setDnpwBefore(bool dnpwBefore);
  bool isEeblAfter() const;
  void setEeblAfter(bool eeblAfter);
  bool isEeblBefore() const;
  void setEeblBefore(bool eeblBefore);
  bool isFcwAfter() const;
  void setFcwAfter(bool fcwAfter);
  bool isFcwBefore() const;
  void setFcwBefore(bool fcwBefore);
  bool isImaAfter() const;
  void setImaAfter(bool imaAfter);
  bool isImaBefore() const;
  void setImaBefore(bool imaBefore);

private:
  bool eeblBefore_;
  bool dnpwBefore_;
  bool fcwBefore_;
  bool imaBefore_;

  bool eeblAfter_;
  bool dnpwAfter_;
  bool fcwAfter_;
  bool imaAfter_;
};

#endif /* SRC_VEINS_MPS_MODULES_UTILITY_V2XAPPFLAGS_H_ */
