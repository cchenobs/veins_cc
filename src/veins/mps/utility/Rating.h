/*
 * Rating.h
 *
 *  Created on: Oct 16, 2018
 *      Author: mps
 */

#ifndef SRC_VEINS_MPS_MODULES_UTILITY_RATING_H_
#define SRC_VEINS_MPS_MODULES_UTILITY_RATING_H_

class Rating
{
public:
  Rating();
  virtual ~Rating();
  double getArt() const;
  void setArt(double art);
  double getKalman() const;
  void setKalman(double kalman);
  double getMan() const;
  void setMan(double maAccelNeg);
  double getMap() const;
  void setMap(double maAccelPos);
  double getMvn() const;
  void setMvn(double maVelNeg);
  double getMvp() const;
  void setMvp(double maVelPos);
  double getMbf() const;
  void setMbf(double mbf);
  double getMdm() const;
  void setMdm(double mdm);
  double getSaw() const;
  void setSaw(double saw);
  double getFbr() const;
  void setFbr(double fbr);
  double getPsa() const;
  void setPsa(double ps);
  double getSa() const;
  void setSa(double sa);
  double getSuspicion() const;
  double getPo() const;
  void setPo(double po);
  double getMdt() const;
  void setMdt(double mdt);
  double getBa() const;
  void setBa(double ba);
  double getLp() const;
  void setLp(double lp);
  double getMp1() const;
  void setMp1(double mp1);
  double getMp2() const;
  void setMp2(double mp2);
  double getMbsm() const;
  void setMbsm(double mbsm);

private:
  double kalman_;
  double art_;
  double saw_;
  double mdm_;
  double mbf_;
  double mvp_;
  double mvn_;
  double map_;
  double man_;
  double fbr_;
  double psa_;
  double sa_;
  double po_;
  double mdt_;
  double ba_;
  double lp_;
  double mp1_;
  double mp2_;
  double mbsm_;
};

#endif /* SRC_VEINS_MPS_MODULES_UTILITY_RATING_H_ */
