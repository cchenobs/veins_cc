/*
 * Latency.cc
 *
 *  Created on: Nov 3, 2018
 *      Author: mps
 */

#include <veins/mps/utility/Latency.h>

Latency::Latency()
: initTime_(Clock::now()),
  initSimTime_(0.0),
  lastPos_(*new Coord()),
  elapsedTime_(0.0),
  nBsm_(1),
  distance_(0.0),
  logFlag_(true)
{}

Latency::
Latency(Coord initPos)
: initTime_(Clock::now()),
  initSimTime_(simTime().dbl()),
  lastPos_(initPos),
  elapsedTime_(0.0),
  nBsm_(1),
  distance_(0.0),
  logFlag_(true)
{}



Latency::
~Latency()
{}


void Latency::
update(Coord curPos) {
  elapsedTime_ = std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - initTime_).count();
  elapsedSimTime_ = simTime().dbl() - initSimTime_;
  nBsm_++;
  distance_ += lastPos_.distance(curPos);
  lastPos_ = curPos;
}

double Latency::getDistance() const
{
  return distance_;
}

double Latency::getElapsedTime() const
{
  return elapsedTime_;
}

double Latency::getBsm() const
{
  return nBsm_;
}

TimePoint Latency::getInitTime() const
{
  return initTime_;
}

bool Latency::isLogFlag() const
{
  return logFlag_;
}

void Latency::setLogFlag(bool logFlag)
{
  logFlag_ = logFlag;
}

double Latency::getElapsedSimTime() const
{
  return elapsedSimTime_;
}

void Latency::setElapsedSimTime(double elapsedSimTime)
{
  elapsedSimTime_ = elapsedSimTime;
}

void Latency::setDistance(double distance)
{
  distance_ = distance;
}

void Latency::setElapsedTime(double elapsedTime)
{
  elapsedTime_ = elapsedTime;
}

void Latency::setBsm(double bsm)
{
  nBsm_ = bsm;
}
