##############################################################################
# Author:       Raashid Ansari
# Year:         2018
# Organization: OnBoard Security
# Description:  This is a Flask framework based server application that hosts
#               the Machine Learning Server. The Machine Learning Model is
#               written in MLModel.py file.
# Notes:        This server is deployed on a proxy gunicorn server which is
#               in-turn deployed on the nginx server.
#               While debugging, this server runs on port 8880, gunicorn
#               proxy server runs on 8000, and nginx server listens to the
#               8880 port to forward requests to this server via gunicorn
##############################################################################

import os
import glob
import shutil

from MLModel import MLModel

from flask import Flask
from flask import request, jsonify
app = Flask(__name__)

model_types = ("ewma", "instant")
base_dir = "{}/src/veins".format(os.environ['HOME'])

@app.route("/ml", methods=['POST'])
def get_prediction():
    data = request.get_json()
    host_vehicle_id = str(data['reporterId'])
    attack_type = data['groundTruth']['attackType']
    selected_attack = data['selectedAttack'].split("/")[1]
    model_type = data['mlType']
    simulation_dir = "{}/examples/mps/results/{}".format(base_dir, selected_attack)

    if model_type not in model_types:
        raise ValueError(
                "Invalid model type. Valid ones are {}".format(modelTypes))

    # check if classifier available for this host vehicle
    # If not, create classifier for that host vehicle
    src = "{}/src/veins/mps/detection_algorithms/classifiers/{}_clf.pkl".format(
            base_dir, model_type)
    classifier = "{}/{}_{}.pkl".format(simulation_dir, model_type, host_vehicle_id)
    if not os.path.isfile(classifier):
        shutil.copy(src, classifier)

    ml_model = MLModel(selected_attack)
    prediction = ml_model.get_prediction(data['ratings'], model_type,
            host_vehicle_id)

    response_object = {
            'status': 'success',
            'data': {
                'prediction': str(prediction)
                }
            }
    return jsonify(response_object), 202

# enable the following to debugging while development
if __name__ == '__main__':
    app.run(debug=False, host='127.0.0.1', port='8000')
