/*
 * SingleThreshold.h
 *
 *  Created on: Aug 30, 2018
 *      Author: mps
 */

#ifndef SRC_VEINS_MPS_DETECTION_ALGORITHMS_SINGLETHRESHOLD_H_
#define SRC_VEINS_MPS_DETECTION_ALGORITHMS_SINGLETHRESHOLD_H_

#include <veins/mps/detection_algorithms/BaseMbrApp.h>

class SingleThreshold : public BaseMbrApp {
public:
  SingleThreshold(const double threshold, Sender& sender);
  virtual ~SingleThreshold();
  virtual bool isMisbehavior();
private:
  const double threshold_;
  Sender& sender_;
};

#endif /* SRC_VEINS_MPS_DETECTION_ALGORITHMS_SINGLETHRESHOLD_H_ */
