/*
 * Ewma.cc
 *
 *  Created on: Sep 4, 2018
 *      Author: mps
 */

#include <veins/mps/detection_algorithms/Ewma.h>

Ewma::
Ewma(const double threshold,
    Sender& sender,
    Checks& checks)
: sender_(sender),
  threshold_(threshold),
  checks_(checks)
{}



Ewma::
~Ewma() {}



bool Ewma::
isMisbehavior() {
  updateRatings();
  return sender_.getEwmaRating().getSuspicion() > threshold_;
}



void Ewma::
updateRatings() {
  auto rating = calcNewRating(checks_.getArt(), sender_.getInstantRating().getArt(), sender_.getEwmaRating().getArt());
  sender_.getEwmaRating().setArt(rating);

  rating = calcNewRating(checks_.getSaw(), sender_.getInstantRating().getSaw(), sender_.getEwmaRating().getSaw());
  sender_.getEwmaRating().setSaw(rating);

  rating = calcNewRating(checks_.getMap(), sender_.getInstantRating().getMap(), sender_.getEwmaRating().getMap());
  sender_.getEwmaRating().setMap(rating);

  rating = calcNewRating(checks_.getMan(), sender_.getInstantRating().getMan(), sender_.getEwmaRating().getMan());
  sender_.getEwmaRating().setMan(rating);

  rating = calcNewRating(checks_.getMvp(), sender_.getInstantRating().getMvp(), sender_.getEwmaRating().getMvp());
  sender_.getEwmaRating().setMvp(rating);

  rating = calcNewRating(checks_.getMvn(), sender_.getInstantRating().getMvn(), sender_.getEwmaRating().getMvn());
  sender_.getEwmaRating().setMvn(rating);

  rating = calcNewRating(checks_.getMbf(), sender_.getInstantRating().getMbf(), sender_.getEwmaRating().getMbf());
  sender_.getEwmaRating().setMbf(rating);

  rating = calcNewRating(checks_.getFbr(), sender_.getInstantRating().getFbr(), sender_.getEwmaRating().getFbr());
  sender_.getEwmaRating().setFbr(rating);

  rating = calcNewRating(checks_.getKalman(), sender_.getInstantRating().getKalman(), sender_.getEwmaRating().getKalman());
  sender_.getEwmaRating().setKalman(rating);

  rating = calcNewRating(checks_.getPsa(), sender_.getInstantRating().getPsa(), sender_.getEwmaRating().getPsa());
  sender_.getEwmaRating().setPsa(rating);

  rating = calcNewRating(checks_.getSa(), sender_.getInstantRating().getSa(), sender_.getEwmaRating().getSa());
  sender_.getEwmaRating().setSa(rating);

  rating = calcNewRating(checks_.getPo(), sender_.getInstantRating().getPo(), sender_.getEwmaRating().getPo());
  sender_.getEwmaRating().setPo(rating);

  rating = calcNewRating(checks_.getMdt(), sender_.getInstantRating().getMdt(), sender_.getEwmaRating().getMdt());
  sender_.getEwmaRating().setMdt(rating);

  rating = calcNewRating(checks_.getBa(), sender_.getInstantRating().getBa(), sender_.getEwmaRating().getBa());
  sender_.getEwmaRating().setBa(rating);

  rating = calcNewRating(checks_.getLp(), sender_.getInstantRating().getLp(), sender_.getEwmaRating().getLp());
  sender_.getEwmaRating().setLp(rating);

  rating = calcNewRating(checks_.getMp1(), sender_.getInstantRating().getMp1(), sender_.getEwmaRating().getMp1());
  sender_.getEwmaRating().setMp1(rating);

  rating = calcNewRating(checks_.getMp2(), sender_.getInstantRating().getMp2(), sender_.getEwmaRating().getMp2());
  sender_.getEwmaRating().setMp2(rating);
}

//TODO: add VEBAS paper link here
double Ewma::
calcNewRating(const ICheck& check, const int newRating, const double oldRating) {
  if (newRating == 0)
    return 0.0; // assuming that this module will return non-zero value for newRating != 0

  auto ewma = oldRating;
  for (int i = 0; i < check.getGain(); ++i) {
    ewma = check.getFadeFactor() * newRating + (1 - check.getFadeFactor()) * ewma; // EWMA from VEBAS paper
  }
  return ewma;
}
