//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MPS_DETECTION_ALGORITHMS_ML_H_
#define SRC_VEINS_MPS_DETECTION_ALGORITHMS_ML_H_

#include <veins/mps/detection_algorithms/BaseMbrApp.h>

class ML : public BaseMbrApp
{
public:
  ML(Sender& sender, int reporter, const std::string& pathPrefix, Checks& checks, std::string& uri, std::string mlType, Rating& ratings);
  virtual ~ML();
  bool isMisbehavior();
private:
  Sender& sender_;
  const int reporter_;
  const std::string& pathPrefix_;
  Checks& checks_;
  std::string& uri_;
  std::string mlType_;
  Rating& ratings_;
private:
  std::string sendRequest();
  std::string makeSenderData();
};

#endif /* SRC_VEINS_MPS_DETECTION_ALGORITHMS_ML_H_ */
