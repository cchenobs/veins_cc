//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/traffic/TrafficManager.h>

Define_Module(TrafficManager);

TrafficManager::
TrafficManager() {}



TrafficManager::
~TrafficManager() {}



void TrafficManager::
initialize(int stage) {
  BaseTrafficManager::initialize(stage);
  if (stage == 0) {
    nAttackers_    = par("nAttackers").intValue();
    nGenuines_     = par("nGenuines").intValue();
    ghostsPerAttacker_ = par("ghostsPerAttacker").intValue();
    arrivalTime_ = par("arrivalTime").doubleValue();

    BaseTrafficManager::setRandomize(par("randomize").boolValue());

    addCar_ = new cMessage("addCar");
    scheduleAt(1.0, addCar_);
  }
}



void TrafficManager::
finish() {
  if (addCar_->isScheduled()) {
    cancelAndDelete(addCar_);
    addCar_ = 0;
  } else if (addCar_) {
    delete(addCar_);
  }

  BaseTrafficManager::finish();
}



void TrafficManager::
handleMessage(cMessage* msg) {
  if (msg == addCar_) {
    createCars();
  } else {
    error("Traffic Manager received unknown Message: %s", msg->getName());
  }
}



void TrafficManager::
createCars() {
  const auto CAR_CREATE_ORDER = par("carCreateOrder").intValue();
  switch(CAR_CREATE_ORDER) {
  case ATTACKER_CAR_FIRST: {
    if (nAttackers_ > 0) {
      createVehicle(ATTACKER, nAttackers_);
      nAttackers_--;
      scheduleAt(getArrivalTime(), addCar_);
    } else if (nGenuines_ > 0) {
      createVehicle(GENUINE, nGenuines_);
      nGenuines_--;
      scheduleAt(getArrivalTime(), addCar_);
    }
    break;
  }

  case GENUINE_CAR_FIRST: {
    if (nGenuines_ > 0) {
      createVehicle(GENUINE, nGenuines_);
      nGenuines_--;
      scheduleAt(getArrivalTime(), addCar_);
    } else if (nAttackers_ > 0) {
      createVehicle(ATTACKER, nAttackers_);
      nAttackers_--;
      scheduleAt(getArrivalTime(), addCar_);
    }
    break;
  }

  case RANDOM_SELECTION: {
    auto whichCar = -1;

    if (nGenuines_ + nAttackers_ > 0)
      whichCar = intrand(2, 0); // Unit test here to check if value is only between 0 or 1

    if (whichCar == -1)
      error("Invalid car option OR all cars already created.");

    if (whichCar == 0 && nGenuines_ > 0) {
      createVehicle(GENUINE, nGenuines_);
      nGenuines_--;
    } else if (whichCar == 1 && nAttackers_ > 0) {
      createVehicle(ATTACKER, nAttackers_);
      nAttackers_--;
    }

    if (nGenuines_ + nAttackers_ > 0)
      scheduleAt(getArrivalTime(), addCar_);
    break;
  }
  default:
    error("Invalid input for car creation order.");
    break;
  }
}



long TrafficManager::
getGhostCarPerAttacker() const {
  return this->ghostsPerAttacker_;
}

simtime_t TrafficManager::
getArrivalTime() {
  // randomize arrival time of vehicles to arrivalTime_ + [0.05, 5)
  auto arrivalTime = simTime() + arrivalTime_ + fmod(dblrand(0) + 0.01, 0.182);
  return arrivalTime;
}
