//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GHOSTCARS_TRAFFICMANAGER_H_
#define __GHOSTCARS_TRAFFICMANAGER_H_

#include <omnetpp/simtime_t.h>
#include <map>
#include <ctime>

#include "veins/mps/traffic/BaseTrafficManager.h"

/**
 * Class to create and control vehicles in a scenario
 */
class TrafficManager : public BaseTrafficManager
{
public:
  TrafficManager();
  ~TrafficManager();
  virtual void initialize      (int stage);
  virtual void finish        ();
  long     getGhostCarPerAttacker() const;
  simtime_t  getArrivalTime();

protected:
  long    nAttackers_;
  long    nGenuines_;
  long    ghostsPerAttacker_;
  cMessage*   addCar_;
  enum CarCreationOrder {
    ATTACKER_CAR_FIRST = -1,
    GENUINE_CAR_FIRST = 0,
    RANDOM_SELECTION = 1,
  };

protected:
  void     createCars   ();
  virtual void handleMessage(cMessage* msg);

private:
  double arrivalTime_;
};

#endif
