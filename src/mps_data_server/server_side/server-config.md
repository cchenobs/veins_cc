# Dependencies

* nginx
* gunicorn

# Installing server configurations

* Install `nginx` and `gunicorn` servers using:

    sudo apt install nginx
    pip3 install gunicorn

* Create a symlink to `mps-server` in `/etc/nginx/sites-available`
* Create a symlink to `/etc/nginx/sites-available/mps-server` in
`/etc/nginx/sites-enabled/`

    cd /etc/nginx/sites-available
    sudo ln -s ~/src/veins/server-configs/mps-server
    cd /etc/nginx/sites-enabled
    sudo ln -s /etc/nginx/sites-available/mps-server

