from flask import Flask
from flask_bootstrap import Bootstrap
from flask_socketio import SocketIO

from server import config
from websocket.streaming import bootstrap_on_connect


# instantiate the extensions
bootstrap = Bootstrap()


def create_app(script_info=None):

    # instantiate the app
    app = Flask(
        __name__,
        template_folder='../client/templates',
        static_folder='../client/static'
    )

    # set config
    app_settings = config.DevelopmentConfig
    app.config.from_object(app_settings)

    # set up extensions
    bootstrap.init_app(app)

    # register blueprints
    from server.views import main_blueprint
    app.register_blueprint(main_blueprint)

    # connect socketio
    socketio = SocketIO(app, message_queue=app.config['REDIS_URL'])
    # socketio.on_event('connect', bootstrap_on_connect)

    # shell context for flask cli
    app.shell_context_processor({'app': app})

    return socketio, app

