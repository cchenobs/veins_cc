bind='localhost:8000'
pidfile='gunicorn_pid'
reload = True
# daemon = True
accesslog='gunicorn_access.log'
errorlog='gunicorn_error.log'
workers=1
worker_class='geventwebsocket.gunicorn.workers.GeventWebSocketWorker'
