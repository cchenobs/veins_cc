function getWindow(lastDate) {
    var window = $('meta[name=x_window]').attr("content");
    var lastDateObj = new Date(lastDate);
    var windowDateObj = lastDateObj.setSeconds(lastDateObj.getSeconds() - window);
    return windowDateObj;
}

function makeExecTimePlot(){
  var additional_params = {
    responsive: true
  };

  var layout = {};

  // Setup plots
  var artPlot = {
    type: 'scatter'
  };
  var artDiv = document.getElementById("exec-time-art-plot");
  Plotly.react(artDiv, artPlot, layout, additional_params);

  var sawPlot = {
    type: 'scatter'
  };
  var sawDiv = document.getElementById("exec-time-saw-plot");
  Plotly.react(sawDiv, sawPlot, layout, additional_params);

  var mbfPlot = {
    type: 'scatter'
  };
  var mbfDiv = document.getElementById("exec-time-mbf-plot");
  Plotly.react(mbfDiv, mbfPlot, layout, additional_params);

  var mvpPlot = {
    type: 'scatter'
  };
  var mvpDiv = document.getElementById("exec-time-mvp-plot");
  Plotly.react(mvpDiv, mvpPlot, layout, additional_params);

  var mvnPlot = {
    type: 'scatter'
  };
  var mvnDiv = document.getElementById("exec-time-mvn-plot");
  Plotly.react(mvnDiv, mvnPlot, layout, additional_params);

  var mapPlot = {
    type: 'scatter'
  };
  var mapDiv = document.getElementById("exec-time-map-plot");
  Plotly.react(mapDiv, mapPlot, layout, additional_params);

  var manPlot = {
    type: 'scatter'
  };
  var manDiv = document.getElementById("exec-time-man-plot");
  Plotly.react(manDiv, manPlot, layout, additional_params);

  var kalmanPlot = {
    type: 'scatter'
  };
  var kalmanDiv = document.getElementById("exec-time-kalman-plot");
  Plotly.react(kalmanDiv, kalmanPlot, layout, additional_params);

  var fbrPlot = {
    type: 'scatter'
  };
  var fbrDiv = document.getElementById("exec-time-fbr-plot");
  Plotly.react(fbrDiv, fbrPlot, layout, additional_params);

  var totalPlot = {
    type: 'scatter'
  };
  var totalDiv = document.getElementById("exec-time-total-plot");
  Plotly.react(totalDiv, totalPlot, layout, additional_params);

  var checkBoxPlot = {
    type: 'box'
  };
  var checkBoxDiv = document.getElementById("exec-time-check-box-plot");
  Plotly.react(checkBoxDiv, checkBoxPlot, layout, additional_params);

  var kalmanBoxPlot = {
    type: 'box'
  };
  var kalmanBoxDiv = document.getElementById("exec-time-kalman-box-plot");
  Plotly.react(kalmanBoxDiv, kalmanBoxPlot, layout, additional_params);

  var totalBoxPlot = {
    type: 'box'
  };
  var totalBoxDiv = document.getElementById("exec-time-total-box-plot");
  Plotly.react(totalBoxDiv, totalBoxPlot, layout, additional_params);

  var mlPlot = {
    type: 'scatter'
  };
  var mlDiv = document.getElementById("exec-time-ml-plot");
  Plotly.react(mlDiv, mlPlot, layout, additional_params);

  var mlBoxPlot = {
    type: 'box'
  };
  var mlBoxDiv = document.getElementById("exec-time-ml-box-plot");
  Plotly.react(mlBoxDiv, mlBoxPlot, layout, additional_params);


    //var windowDateObj = getWindow(x[x.length - 1])
    //var layout = {
      //grid: { rows: 3, columns: 3, pattern: 'independent' },
      //font: { size: 18 },
      //margin: { t: 0 },
      //xaxis: {
        //range: [windowDateObj,  x[x.length - 1]],
        //rangeslider: {range: [x[0], x[x.length - 1]]},
        //type: 'scatter'
      //},
      //yaxis: {
        //range: [0, 110]
      //}
    //};
}


function updatePlot(x, y, title, plot_name) {
  var i = 0;
  var data = new Array();
  for (i = 0; i < x.length; i++) {
    data.push(
        {
          x: x[i],
          y: y[i],
          type: 'scatter'
        }
      );
  }

  var layout = {
    title: title
  }

  var plotDiv = document.getElementById(plot_name);
  Plotly.react(plotDiv, data, layout);
}


function updateBoxPlot(x, y, title, plot_name) {
  var i = 0;
  var data = new Array();
  for (i = 0; i < x.length; i++) {
    data.push(
        {
          x: x[i],
          y: y[i],
          type: 'box'
        }
      );
  }

  var layout = {
    title: title
  }

  var plotDiv = document.getElementById(plot_name);
  Plotly.react(plotDiv, data, layout);
}


var url = 'http://' + document.domain + ':' + location.port
var socket = io.connect(url);

socket.on('connect', function(msg) {
  console.log('connected to websocket on ' + url);
});

socket.on('bootstrap', function (msg) {
  plot_start = msg.x[0];
  console.log('bootstrapping');
});
 
socket.on('setup-exec-time-plot', function (msg) {
  makeExecTimePlot();
  console.log('creating exec-time plot')
})

socket.on('update-art-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'ART', 'exec-time-art-plot');
  console.log('updating art plot');
});

socket.on('update-saw-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'SAW', 'exec-time-saw-plot');
  console.log('updating saw plot');
});

socket.on('update-mbf-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'MBF', 'exec-time-mbf-plot');
  console.log('updating mbf plot');
});

socket.on('update-mvp-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'MVP', 'exec-time-mvp-plot');
  console.log('updating mvp plot');
});

socket.on('update-mvn-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'MVN', 'exec-time-mvn-plot');
  console.log('updating mvn plot');
});

socket.on('update-map-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'MAP', 'exec-time-map-plot');
  console.log('updating map plot');
});

socket.on('update-man-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'MAN', 'exec-time-man-plot');
  console.log('updating man plot');
});

socket.on('update-kalman-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'KALMAN', 'exec-time-kalman-plot');
  console.log('updating kalman plot');
});

socket.on('update-fbr-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'FBR', 'exec-time-fbr-plot');
  console.log('updating fbr plot');
});

socket.on('update-total-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'Total', 'exec-time-total-plot');
  console.log('updating total plot');
});

socket.on('update-ml-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'ML', 'exec-time-ml-plot');
  console.log('updating ml plot');
});

socket.on('update-check-box-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updateBoxPlot(parsed.x, parsed.y, 'Checks', 'exec-time-check-box-plot');
  console.log('updating check box plot');
});

socket.on('update-kalman-box-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updateBoxPlot(parsed.x, parsed.y, 'KALMAN', 'exec-time-kalman-box-plot');
  console.log('updating kalman box plot');
});

socket.on('update-total-box-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updateBoxPlot(parsed.x, parsed.y, 'TOTAL', 'exec-time-total-box-plot');
  console.log('updating total box plot');
});

socket.on('update-ml-box-exec-time-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updateBoxPlot(parsed.x, parsed.y, 'ML', 'exec-time-ml-box-plot');
  console.log('updating ml box plot');
});

