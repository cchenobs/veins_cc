function getWindow(lastDate) {
    var window = $('meta[name=x_window]').attr("content");
    var lastDateObj = new Date(lastDate);
    var windowDateObj = lastDateObj.setSeconds(lastDateObj.getSeconds() - window);
    return windowDateObj;
}

function makeAttackPlot(){
  var additional_params = {
    responsive: true
  };

  var layout = {};

  // Setup plots
  var baseConfusionPlot = {
    name: 'Count',
    type: 'bar'
  };

  var baseConfusionPlotDiv = document.getElementById("attack-base-confusion-plot");
  Plotly.react(baseConfusionPlotDiv, baseConfusionPlot, layout, additional_params);

  var rocPlot = {
    name: 'ROC',
    type: 'scatter'
  };

  var rocPlotDiv = document.getElementById("attack-roc-plot");
  Plotly.react(rocPlotDiv, rocPlot, layout, additional_params);

  var prPlot = {
    name: 'PR',
    type: 'scatter'
  };

  var prPlotDiv = document.getElementById("attack-pr-plot");
  Plotly.react(prPlotDiv, prPlot, layout, additional_params);

  var accuracyPlot = {
    name: 'Accuracy',
    type: 'scatter'
  };

  var accuracyPlotDiv = document.getElementById("attack-accuracy-plot");
  Plotly.react(accuracyPlotDiv, accuracyPlot, layout, additional_params);

  var precisionPlot = {
    name: 'Precision',
    type: 'scatter'
  };

  var precisionPlotDiv = document.getElementById("attack-precision-plot");
  Plotly.react(precisionPlotDiv, precisionPlot, layout, additional_params);

  var recallPlot = {
    name: 'Recall',
    type: 'scatter'
  };

  var recallPlotDiv = document.getElementById("attack-recall-plot");
  Plotly.react(recallPlotDiv, recallPlot, layout, additional_params);

  var f1ScorePlot = {
    name: 'F1 Score',
    type: 'scatter'
  };

  var f1ScorePlotDiv = document.getElementById("attack-f1score-plot");
  Plotly.react(f1ScorePlotDiv, f1ScorePlot, layout, additional_params);

  var vehicleDensityPlot = {
    name: 'Vehicle Density',
    type: 'scatter'
  };

  var vehicleDensityPlotDiv = document.getElementById("attack-vehicle-density-plot");
  Plotly.react(vehicleDensityPlotDiv, vehicleDensityPlot, layout, additional_params);

    //var windowDateObj = getWindow(x[x.length - 1])
    //var layout = {
      //grid: { rows: 3, columns: 3, pattern: 'independent' },
      //font: { size: 18 },
      //margin: { t: 0 },
      //xaxis: {
        //range: [windowDateObj,  x[x.length - 1]],
        //rangeslider: {range: [x[0], x[x.length - 1]]},
        //type: 'scatter'
      //},
      //yaxis: {
        //range: [0, 110]
      //}
    //};
};

var plot_start = 0;
var firstTime = true

function updateBaseConfusionPlot(x, y) {
  var i = 0;
  var data = new Array();
  var name_list = ['TP', 'FP', 'FN', 'TN'];
  var color_list = ['red', 'blue', 'green', 'yellow'];

  if (name_list.length != y.length) {
    console.log('length not same for name_list and y');
  }

  if (name_list.length != color_list.length) {
    console.log('length not same for name_list and color_list');
  }

  for (i = 0; i < y.length; i++) {
    data.push(
        {
          x: x,
          y: y[i],
          type: 'bar',
          name: name_list[i],
          marker: {
            color: color_list[i],
            opacity: 0.7
          }
        }
      );
  }

  var layout_update = {
    title: "Confusion",
    xaxis: {
      tickangle: 90,
      tickfont: {
        size: 10
      }
    },
    yaxis: {
      zeroline: false,
      gridwidth: 2
    },
    barmode: 'stack'
  }

  // plot
  var plotDiv = document.getElementById("attack-base-confusion-plot");
  if (firstTime) {
    //firstTime = false;
    Plotly.react(plotDiv, data, layout_update);//, [0]);
  } else {
    Plotly.extendTraces(plotDiv, data, [0]);
  }
};


function updatePlot(x, y, title, plot_name) {
  var i = 0;
  var data = new Array();
  for (i = 0; i < x.length; i++) {
    data.push(
        {
          x: x[i],
          y: y[i],
          type: 'scatter'
        }
      );
  }

  var layout = {
    title: title
  }

  var plotDiv = document.getElementById(plot_name);
  Plotly.react(plotDiv, data, layout);
}


var url = 'http://' + document.domain + ':' + location.port
var socket = io.connect(url);

socket.on('connect', function(msg) {
  console.log('connected to websocket on ' + url);
});

socket.on('bootstrap', function (msg) {
  plot_start = msg.x[0];
  console.log('bootstrapping');
});

socket.on('setup-attack-plot', function (msg) {
  makeAttackPlot();
  console.log('creating attack plot')
})

socket.on('update-attack-base-confusion-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updateBaseConfusionPlot( parsed.x, parsed.y );
  console.log('updating base confusion plot');
});

socket.on('update-attack-roc-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'ROC', 'attack-roc-plot');
  console.log('updating roc plot');
});

socket.on('update-attack-pr-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'PR', 'attack-pr-plot');
  console.log('updating pr plot');
});

socket.on('update-attack-precision-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'Precision', 'attack-precision-plot');
  console.log('updating precision plot');
});

socket.on('update-attack-recall-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'Recall', 'attack-recall-plot');
  console.log('updating recall plot');
});

socket.on('update-attack-f1score-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'F1-Score', 'attack-f1score-plot');
  console.log('updating f1score plot');
});

socket.on('update-attack-vehicle-density-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'Vehicle Density', 'attack-vehicle-density-plot');
  console.log('updating vehicle-density plot');
});

