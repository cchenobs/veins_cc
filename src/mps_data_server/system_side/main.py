#! /usr/bin/env python3

###############################################
# Author: Raashid Ansari
# Year: 2018
# Latest Revision: 01/16/2019
# Organization: OnBoard Security, Inc.
# Description: GUI for system side of MPS
###############################################

import re
import os
import glob
import shutil
import signal
import socket
import requests
from contextlib import contextmanager

from PyQt4 import QtGui, QtCore, QtTest, uic

base_dir = os.environ['HOME'] + "/src/veins/"
qtCreatorFile = base_dir + "src/mps_data_server/system_side/gui.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

ini_files_path = base_dir + "examples/mps/*.ini"
base_log_path = base_dir + "examples/mps/results/"

class TimeOutError(Exception):
    pass
 
class MpsApp(QtGui.QMainWindow, Ui_MainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        self.log_path = base_log_path
        self.configFile = ""
        self.hostTextEdit.setText("http://localhost")
        self.portTextEdit.setText("8000")

        self.getCities()
        self.updateServerAddress()
        self._omnet_process = QtCore.QProcess(self)
        self._timer = QtCore.QTimer(self)
        self.connectEvents()
        self.stopButton.setDisabled(True)


    def getCities(self):
        # extract list of unique cities from file names of configuration
        # ini files
        cities = [c for c in {re.search(r"(\w+?)((No)?Gui)?\.ini",
            city.split("/")[-1]).group(1)
            for city in glob.glob(ini_files_path) if not "omnetpp" in
            city}]
        cities.sort()
        self.cityComboBox.clear()
        self.cityComboBox.addItems(cities)


    def closeEvent(self, event):
        if self._timer.isActive():
            self._timer.stop()
            self._omnet_process.close()

        QtGui.QWidget.closeEvent(self, event)


    def connectEvents(self):
        # buttons
        self.startButton.clicked.connect(self.startSimulation)
        self.stopButton.clicked.connect(self.stopSimulation)

        # timers
        # self._timer.timeout.connect(self._writeSystemResourceUsage)

        # text edits
        self.hostTextEdit.textChanged.connect(self.updateServerAddress)
        self.portTextEdit.textChanged.connect(self.updateServerAddress)

        # combo-boxes
        self.attackComboBox.currentIndexChanged.connect(self.updateLogPath)
        self.cityComboBox.activated.connect(self.updateSimulationConfig)
        self.cityComboBox.activated.connect(self.addAttacks)

        # checkboxes
        self.guiCheckBox.stateChanged.connect(self.updateSimulationConfig)
        self.guiCheckBox.stateChanged.connect(self.addAttacks)

        # slots
        self._omnet_process.readyRead.connect(self.readProcessOutput)
        self._omnet_process.finished.connect(self.stopSimulation)


    def readProcessOutput(self):
        output = str(self._omnet_process.readAll()).split("\\r")
        # print(output)
        for line in output:
            if "Step" and "TOT" and "ACT" and "TraCI" and "BUF" and not "~=" in line.strip():
                tokens = re.findall(r"[\w\d\.#]+", line)
                try:
                    self.logLabel.setText(
                            tokens[0] + ": " + tokens[1]
                            + " Vehicles:: Active: " + tokens[11]
                            + " Departed: " + str(int(tokens[9]) - int(tokens[11]))
                            + " Total: " + tokens[9])
                except IndexError:
                    pass
                except ValueError:
                    pass

    
    def updateLogPath(self):
        self.log_path = base_log_path + self.attackComboBox.currentText()

        if self.log_path == base_log_path:
            self.errorLabel.setText("Select an attack")
        else:
            self.errorLabel.clear()


    def updateSimulationConfig(self):
        self.configFile = self.cityComboBox.currentText().lower()
        if self.guiCheckBox.isChecked():
            self.configFile = self.configFile + "Gui.ini"
        else:
            self.configFile = self.configFile + "NoGui.ini"

        self.log_path = self.log_path + self.attackComboBox.currentText()


    def updateServerAddress(self):
        hostRe = re.compile(r'^(http://)?(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|localhost)$')
        host = self.hostTextEdit.toPlainText()
        if re.search(hostRe, host) is None:
            self.errorLabel.setText("invalid host name")
            return
        else:
            if re.search(hostRe, host).group(0) is None:
                self.errorLabel.setText("invalid host name")
                return
            else:
                self.errorLabel.clear()

            if re.search(hostRe, host).group(1) is None:
                host = "http://" + host

        portRe = re.compile(r'^\d{4}$|^$')
        port = self.portTextEdit.toPlainText()

        if re.search(portRe, port) is None:
            self.errorLabel.setText("invalid port number")
            return
        else:
            if re.search(portRe, port).group(0) is None:
                self.errorLabel.setText("invalid port number")
                return
            else:
                self.errorLabel.clear()
                port = ":" + port

        self.server_address = ""
        if port == ":":
            self.server_address = host
        else:
            self.server_address = host + port


    def addAttacks(self):
        configs = []
        try:
            with open(base_dir + "examples/mps/" + self.configFile, "r") as omnetpp_fh:
                configs = omnetpp_fh.readlines()
                self.errorLabel.clear()
        except FileNotFoundError:
            self.errorLabel.setText("config file not found")

        attacks = [re.findall(r"[\w']+", config)[-1] for config in configs
                if "Config" in config and not "Gui" in config]

        attack = self.attackComboBox.currentText()
        self.attackComboBox.clear()
        self.attackComboBox.addItems(attacks)
        attack_idx = self.attackComboBox.findText(attack)
        self.attackComboBox.setCurrentIndex(attack_idx)


    def startSimulation(self):
        serverAddrRe = re.compile(
                r'^http://(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|localhost)(:(\d{4}$|^$))?$')

        if re.search(serverAddrRe, self.server_address) is None:
            self.errorLabel.setText("Server address invalid")
            return
        else:
            self.errorLabel.clear()

        if self.log_path == base_dir:
            self.errorLabel.setText("Log path invalid")
            return
        else:
            self.errorLabel.clear()

        host = re.search(serverAddrRe, self.server_address).group(1)
        port = re.search(serverAddrRe, self.server_address).group(3)
        if not isMpsServerActive(host, int(port)):
            self.errorLabel.setText("MPS Server is not up")
            return
        else:
            self.errorLabel.clear()

        with cd(base_dir + "/examples/mps"):
            self.executeOmnetCommand()

        self.waitForTraceFiles()


    def executeOmnetCommand(self):
        self.startButton.setDisabled(True)
        self.stopButton.setEnabled(True)
        self.cityComboBox.setDisabled(True)
        self.attackComboBox.setDisabled(True)
        self._timer.start(50)
        try:
            shutil.rmtree(self.log_path)
        except FileNotFoundError:
            error = "Nothing to delete at: " + self.log_path
            print(error)
            self.errorLabel.setText(error)


        run_args = self._buildOppRunArgs()
        self._omnet_process.start(run_args[0], run_args[1:], QtCore.QIODevice.ReadOnly)


    def waitForTraceFiles(self):
        # wait for trace files to be generated
        # initialize timeout timer
        signal.signal(signal.SIGALRM, self._sig_alarm)
        if self.guiCheckBox.isChecked():
            signal.alarm(120)
        else:
            signal.alarm(60)
        try:
            count = 1
            while(not glob.glob(self.log_path + "/*_trace.csv")):
                QtTest.QTest.qWait(2000)
                wait_list = ["Waiting for trace files"]
                wait_list = wait_list + ['.' for i in range(count)]
                count = count + 1
                wait_string = ""
                for item in wait_list:
                    wait_string = wait_string + item
                self.logLabel.setText(wait_string)
        except TimeoutError:
            error = "Timeout reached for trace file lookup"
            print(error)
            self.errorLabel.setText(error)
            self.logLabel.clear()
        # cancel timeout timer
        signal.alarm(0)

    def stopSimulation(self):
        if self._timer.isActive():
            self._timer.stop()
            self._omnet_process.close()
            self.startButton.setEnabled(True)
            self.stopButton.setDisabled(True)
            self.cityComboBox.setEnabled(True)
            self.attackComboBox.setEnabled(True)
            self.errorLabel.clear()
            self.logLabel.clear()
        r = requests.post(self.server_address + "/simulation/stop",
                data={'attack': self.attackComboBox.currentText()})
        print(r)

    # def _writeSystemResourceUsage(self):
        # sru = SystemResourceUsage()
        # try:
            # with open(self.log_path + "/ramUsage.csv", "a") as ramUsage_fh:
                # ramUsage = sru.getRamUsage(self._omnet_process.pid())
                # cpuUsage = sru.getCpuUsage(self._omnet_process.pid())
                # ramUsage_fh.write('{0:.3f},{1:.3f}'.format(ramUsage, cpuUsage) + "\n")
        # except FileNotFoundError:
            # pass


    def _buildOppRunArgs(self):
        run_args = "opp_run -m -n .:../veins:../../src/veins --image-path=../../images -l ../../src/veins -c".split()
        run_args.append(self.attackComboBox.currentText())

        # Handle GUI choice
        run_args.append("-u")
        if self.guiCheckBox.isChecked():
            run_args.append("Qtenv")
        else:
            run_args.append("Cmdenv")
        run_args.append(self.configFile)
        return run_args


    def _sig_alarm(self, sig, tb):
        raise TimeoutError("timeout")


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def isMpsServerActive(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((host, port))
    return True if result == 0 else False


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    window = MpsApp()
    window.show()
    sys.exit(app.exec_())

