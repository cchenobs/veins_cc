#! /bin/bash

printUsage() {
  echo "usage: $0 <nopcsp | fixedpcsp | fixedpc | c2cpc | c2cpcsp> <ld | md | hd>"
  exit 1
}

if [[ $# -lt 2 ]]; then
  printUsage
fi

# select configuration according to command line input
config=""
case "$1" in
  "nopcsp")
    config="No_PC_SP"
    ;;
  "fixedpcsp")
    config="Fixed_PC_and_SP"
    ;;
  "fixedpc")
    config="Fixed_PC_no_SP"
    ;;
  "c2cpc")
    config="C2C_with_PC_no_SP"
    ;;
  "c2cpcsp")
    config="C2C_with_PC_with_SP"
    ;;
  *)
    printUsage
    ;;
esac

case "$2" in
  "ld")
    config+="_LD"
    ;;
  "md")
    config+="_MD"
    ;;
  "hd")
    config+="_HD"
    ;;
esac

# move to folder from where the simulation is to be run
cd $HOME/src/veins/examples/silentPeriod

# run simulation
opp_run -m -u Cmdenv -c $config -n .:../veins:../../src/veins --image-path=../../images -l ../../src/veins omnetpp.ini

